#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>
#include "patch_sta.h"
#include "conv.h"
#include "cmdline.h"
#include "output.h"

void usage(int status)
{
	if (status == 1)
		fprintf(stderr, "Try '%s -h' for more information.\n",
			 program_name);
	else {
		printf("Usage: %s INPUTFILE [options]\n",
			program_name);
		fputs("\n\
 -a		output all the patch info of companies and their person.\n\
 -c COMNAME	output detail patch info of some company.\n\
 -C TOPX	output TOPX company or all company if TOPX is 0.\n\
 -e		Anti Email Spam, this will convert all email addresses to \
coded text. \n\
 -I		output all the patch info of companies and their most active areas.\n\
		do not use with -L option.\n\
 -i		output all the patch info of nations and their most active areas.\n\
		do not use with -L option.\n\
 -l TailLimit	output the longtail of contributor whose patch number are\n\
		less than TailLimit.\n\
 -L		statistic of changed line instead of changeset number.\n\
		do not use with -I or -i option.\n\
 -m PERSON	output some person's rank and patch count.\n\
 -M TOPX	output TOPX person or all person if TOPX is 0.\n\
 -n		do not merge different mail addresses to one person.\n\
		do not merge different domains to one company.\n\
 -N NATION	output all the person who belong to the NATION.\n\
 -O WHAT	output other kind of statistics. Now we support [report], \
[review], [test], [ack] and [sof].\n\
 -r RELEASE	Used with -s option, this is the release name which will be \
inserted into db.\n\
 -s SQLFILE	output all sql format data to file.\n\
 -t HTMLFILE    Output all nation's contribution to HTML file.\n\
 -w		Report warning about potential duplicated developers. \n\
 -x THMLFILE	output all info as HTML file.\n\
", stdout);
		fputs("\
\n\
Exit status is 0 if OK, 1 if trouble.\n\
", stdout);
		printf("\nReport bugs to <%s>.\n", BUGREPORT);
	}
	exit(status);
}

int get_info(struct list_head **com_list_head,
	      struct list_head **per_list_head,
	      const char *tmp_author_file)
{
	FILE *fp_t;
	char *line = NULL;
	size_t len = 0;
	struct list_head *com_list = STRUCT_INIT(struct list_head);
	struct list_head *per_list = STRUCT_INIT(struct list_head);
	char *com_name, *per_name, *mail, *dir_name;
	struct company *current;
	int ret = 0;
	unsigned long long int chgline;

	cnty_list = STRUCT_INIT(struct list_head);

	if ((!com_list) || (!per_list) || (!cnty_list)) {
		ret = 1;
		goto normal_out;
	}
	INIT_LIST_HEAD(com_list);
	INIT_LIST_HEAD(per_list);
	INIT_LIST_HEAD(cnty_list);

	fp_t = fopen(tmp_author_file, "r");
	if (!fp_t) {
		printf("[ERROR]: Can not open changelog file %s.\n",
			tmp_author_file);
		ret = 1;
		goto normal_out;
	}

	while (getline(&line, &len, fp_t) != -1) {
		char *nextline = NULL;
		char *dirline = NULL;
		unsigned int date;
		struct directory *dir_current, *dir;
		struct list_head *dir_list = STRUCT_INIT(struct list_head);
		struct list_head *dir_cur_list, *tmp_list;

		INIT_LIST_HEAD(dir_list);
		if (g_other != SELECT) {
			if (is_author(line) != 1)
				continue;
			/* after author line is diffstat line */
			if (getline(&nextline, &len, fp_t) != -1)
				if (is_diffstat(nextline) != 1)
					continue;
			/* after diffstat line are dirstat lines */
			INIT_LIST_HEAD(dir_list);
			while((getline(&dirline, &len, fp_t) != -1)
			      && is_dirstat(dirline)) {
				dir_from_line(&dir_name, dirline);
				list_for_each_entry(dir_current, dir_list, list)
					if (strcasestr(dir_current->name, dir_name))
						break;
				if (&dir_current->list == dir_list) {
					NODE_INIT(struct directory, dir);
					NODE_SET_NAME(dir, dir_name);
					list_add_tail(&(dir->list), dir_list);
				}
			}
		}

		/* it's author line, we get data from it*/
		if (per_from_line(&per_name, line) != 0) {
			ret = 1;
			goto out_closefile1;
		}

		date = date_from_line(line);
		if (date < 0) {
			ret = 1;
			goto out_closefile1;
		}

		if (mail_from_per(&mail, per_name) < 0) {
			ret = 1;
			goto out_closefile2;
		}

		if (com_from_mail(&com_name, mail) < 0) {
			ret = 1;
			goto out_closefile3;
		}

		if (g_other != SELECT)
			chgline = chg_from_line(nextline);
		else
			chgline = 0;

		if (g_notconv == NSELECT)
			conv_per_com(&per_name, &com_name, date);

		list_for_each_entry(current, com_list, list)
			if (memcmp(current->name, com_name,
				    strlen(com_name)+1) == 0)
					break;
		if (&(current->list) == com_list) {
			if (new_com(com_list, per_list, com_name, per_name,
				    dir_list, chgline)) {
				ret = 1;
				goto out;
			}
		} else {
			if (update_com(current, com_list, per_list, per_name,
				       dir_list, chgline)) {
				ret = 1;
				goto out;
			}
		}

		free(com_name);
		com_name = NULL;
		free(per_name);
		per_name = NULL;
		free(line);
		line = NULL;
		free(nextline);
		list_for_each_safe(dir_cur_list, tmp_list, dir_list) {
			dir_current = list_entry(dir_cur_list, struct directory, list);
			list_del(dir_cur_list);
			free(dir_current);
		}
	}

	*com_list_head = com_list;
	*per_list_head = per_list;

out:
	if (per_name)
		free(per_name);
out_closefile3:
	if (mail)
		free(mail);
out_closefile2:
	if (per_name)
		free(per_name);
out_closefile1:
	if (line)
		free(line);
	fclose(fp_t);
normal_out:
	return ret;
}

int main(int argc, char **argv)
{
	struct list_head *com_list = NULL, *per_list = NULL;
	int ret = 0;

	if (parse_command_line(argc, argv) != OPT_OK)
		usage(1);

	if (g_help == SELECT)
		usage(0);

	if (get_info(&com_list, &per_list, filename_i)) {
		ret = 1;
		goto out;
	}

	strncpy(statistics_msg, str_patch, sizeof(statistics_msg));
	if (g_line == SELECT) 
		strncpy(statistics_msg, str_line, sizeof(statistics_msg));

	if (g_other == SELECT) {
		switch (other) {
		case OTHER_REPORT:
			strncpy(statistics_msg, str_report,
				sizeof(statistics_msg));
			break;
		case OTHER_REVIEW:
			strncpy(statistics_msg, str_review,
				sizeof(statistics_msg));
			break;
		case OTHER_TEST:
			strncpy(statistics_msg, str_test,
				sizeof(statistics_msg));
			break;
		case OTHER_ACK:
			strncpy(statistics_msg, str_ack,
				sizeof(statistics_msg));
			break;
		case OTHER_SOF:
			strncpy(statistics_msg, str_sof,
				sizeof(statistics_msg));
			break;
		default:
			break;
		}
	}

	if (g_all == SELECT) {
		print_all(com_list);
	}

	if (g_comtop == SELECT)
		print_com_rank(com_list, (unsigned int)p_comtop);

	if (g_com == SELECT)
		print_com_detail(com_list, p_com);

	if (g_cnty == SELECT)
		print_cnty(cnty_list, p_cnty);

	if (g_tail == SELECT)
		print_longtail(per_list, p_tail);

	if (g_nation == SELECT)
		print_per_cnt(per_list, p_nation);

	if (g_interest == SELECT)
		print_com_int(com_list);

	if (g_nat_interest == SELECT)
		print_cnty_int(cnty_list, p_nat_interest);

	if (g_person == SELECT)
		print_per_detail(per_list, p_person);

	if (g_pertop == SELECT)
		print_per_rank(per_list, (unsigned int)p_pertop);

	if (g_html == SELECT) {
		if (g_interest == SELECT) {
			if (print_com_int_html(com_list, p_html))
				ret = 1;
		} else {
			if (print_html(com_list, p_html))
				ret = 1;
		}
	}

	if (g_sql == SELECT)
		if (print_sql(com_list, p_sql, p_rls))
			ret = 1;
out:
	if (com_list)
		free(com_list);
	if (per_list)
		free(per_list);
	if (cnty_list)
		free(cnty_list);

	return ret;
}
