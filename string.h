#ifndef INC_STRING
#define INC_STRING

#include <ctype.h>

static inline int init_str(char **str, int len)
{
	*str = (char *)malloc(len);
	if ((*str) == NULL) {
		printf("[ERROR]: No enough mem\n");
		return 1;
	}
	memset(*str, (int)'\0', len);
	return 0;
}

/* get name from author info. it is the part of per_name except "<...>" part
 */
static int name_from_per(char **name, const char *per_name)
{
	char *begin = (char *)per_name;
	int len = 0;

	while (((*per_name) != '<') && ((*per_name) != '\0')) {
		per_name++;
		len++;
	}

	if (init_str(name, len+1))
		return -1;
	memcpy(*name, begin, len);

	return len;
}

/* Get the first name from a mail infor.
 * return first name length.
 */
int firstname(char **first, const char *mail)
{
	char *name, *tmp, *start;
	int len = 0;

	if (name_from_per(&name, mail) <= 0)
		return 0;
	tmp = name;

	/* find the first non blank char */
	while (((*tmp) != '\0') && ((*tmp) == ' ')) {
		tmp++;
	}
	if (*tmp == '\0')	return 0;

	start = tmp;
	while (((*tmp) != '\0') && ((*tmp) != ' ')) {
		tmp++;
		len++;
	}

	if (init_str(first, len+1))
		return 0;
	memcpy(*first, start, len);
	return len;
}

/* Get the mid name from a mail infor.
 * return mid name length.
 */
int midname(char **mid, const char *mail)
{
	char *name, *tmp, *start;
	int len = 0;

	if (name_from_per(&name, mail) <= 0)
		return 0;
	tmp = name;

	/* find the first non blank char */
	while (((*tmp) != '\0') && ((*tmp) == ' ')) {
		tmp++;
	}
	if (*tmp == '\0')	return 0;
	while (((*tmp) != '\0') && ((*tmp) != ' ')) {
		tmp++;
	}
	if (*tmp == '\0')	return 0;

	/* continue from first blank */
	while (((*tmp) != '\0') && ((*tmp) == ' ')) {
		tmp++;
	}
	if (*tmp == '\0')	return 0;
	start = tmp;
	while (((*tmp) != '\0') && ((*tmp) != ' ')) {
		tmp++;
		len++;
	}
	if (*tmp == '\0')	return 0;

	/* find next part of name*/
	while (((*tmp) != '\0') && ((*tmp) == ' ')) {
		tmp++;
	}
	if (*tmp == '\0')	return 0;

	if (init_str(mid, len+1))
		return 0;
	memcpy(*mid, start, len);
	return len;
}

/* Get the last name from a mail infor.
 * return last name length.
 */
int lastname(char **last, const char *mail)
{
	char *name, *tmp, *start;
	int len = 0;

	if (name_from_per(&name, mail) <= 0)
		return 0;
	tmp = name;

	/* find the first non blank char */
	while (((*tmp) != '\0') && ((*tmp) == ' ')) {
		tmp++;
	}
	if (*tmp == '\0')	return 0;
	start = tmp;

	while (*tmp != '\0') {
		if (*tmp == ' ') {
			while ((*tmp != '\0') && (*tmp == ' ')) {
				tmp++;
			}
			if (*tmp == '\0')	break;
			start = tmp;
		}
		tmp++;
	}

	/* start points to last word */
	tmp = start;
	while (((*tmp) != '\0') && ((*tmp) != ' ')) {
		tmp++;
		len++;
	}
	if (init_str(last, len+1))
		return 0;
	memcpy(*last, start, len);
	return len;
}

/* get mail address from author info, mail address format is:
 * <abc@smt.com>
 */
static int mail_from_per(char **mail_name, const char *per_name)
{
	char *begin = NULL;
	int len = 0;

	while (((*per_name) != '<') && ((*per_name) != '\0'))
		per_name++;
	begin = (char *)(++per_name);
	begin--;

	while (((*per_name) != '>') && ((*per_name) != '\0')) {
		per_name++;
		len++;
	}
	len = len+2;

	if (init_str(mail_name, len+1))
		return -1;
	memcpy(*mail_name, begin, len);

	return len;
}

static int com_from_mail(char **com, const char *mail)
{
	char *begin;
	size_t len = 0;

	while ((mail !=NULL) && ((*mail) != '@') &&
		((*mail) != '\n') && ((*mail) != '\0'))
		mail++;
	begin = (char *)(++mail);

	while ((mail !=NULL) && ((*mail) != '>') &&
		((*mail) != '\n') && ((*mail) != '\0')) {
		mail++;
		len++;
	}

	if (init_str(com, len+1))
		return -1;
	memcpy(*com, begin, len);

	return len;
}

/* Get the account name from a mail infor.
 * return account name length.
 */
int account(char **account, const char *mail)
{
	char *email, *tmp;
	int len = 0;

	if (mail_from_per(&email, mail) <= 0)
		return 0;

	/* ignore '<' */
	email++;
	tmp = email;
	while (((*tmp) != '\0') && ((*tmp) != '@')) {
		tmp++;
		len++;
	}
	if (init_str(account, len+1))
		return 0;
	memcpy(*account, email, len);

	return len;
}

/* Get the domain name from a mail infor.
 * return domain name length.
 */
int domain(char **domain, const char *mail)
{
	char *email;
	int len;

	if (mail_from_per(&email, mail) <= 0)
		return 0;
	len = com_from_mail(domain, email);
	return len;
}

/* This function get engineer's infor from one log line.
 * Note: When get info from Reviewed-by, Acked-by, etc. We use this function
 *       too. So, don't hardcode to find the position of engineer's info.
 * Current format is:
Author: Wang Chen <wangchen@cn.fujitsu.com>; Date: 2009-01-14
 */
static int per_from_line(char **per, char *line)
{
	char *begin;
	size_t len = 0;

	while ((*line) != ':')
		line++;
	while ((*line) != ' ')
		line++;
	begin = ++line;

	while (((*line) != '>') && ((*line) != '\n') &&
		((*line) != ';') && ((*line) != '\0')) {
		line++;
		len++;
	}
	if ((*line) == '>')
		len++;

	if ((strstr(begin, "<") == NULL) && (strstr(begin, ">") == NULL)) {
		/* If get a non-mail string, cast it to mail format. */
		if (init_str(per, len+3))
			return 1;
		memcpy(*per+1, begin, len);
		**per = '<';
		(*per)[len+1] = '>';
	} else {
		if (init_str(per, len+1))
			return 1;
		memcpy(*per, begin, len);
	}
	return 0;
}

/* Get commit's author date for further use, such as employ attribution.
 * Return value: the date as a number, for example:
 * 			2007-05-12 -> 20070512
 *		   -1 if any error happen
 */
static int date_from_line(char *line)
{
	unsigned int date;
	char *str;
	char *head;
	int i;

	while ((*line) != ';' && (*line) != '\0')
		line++;
	/* some changelog hasn't date, for example, sof changelog */
	if ((*line) == '\0') {
		date = 0;
		return date;
	}

	while ((*line) != ':')
		line++;
	line += 2;

	/* Hardcode here to strictly check the date format. */
	if (init_str(&str, 9))
		return -1;
	head = str;
	for (i = 0; i < 4; i++) {
		if (isdigit(*line))
			*str = *line;
		else
			goto error;
		line++;
		str++;
	}
	line++;
	for (i = 0; i < 2; i++) {
		if (isdigit(*line))
			*str = *line;
		else
			goto error;
		line++;
		str++;
	}
	line++;
	for (i = 0; i < 2; i++) {
		if (isdigit(*line))
			*str = *line;
		else
			goto error;
		line++;
		str++;
	}
	*str = '\0';
	date = strtod(head, NULL);
	free(head);
	return date;
error:
	free(head);
	return -1;
}

static unsigned long long int __chg_from_line(char **nextline)
{
	char num[8];
	int i = 0;
	while (**nextline != ',')
		(*nextline)++;
	*nextline = (*nextline)+2;	/* now *nextline points to number */
	memset(num, (int)'\0', 8);
	while (**nextline != ' ') {
		num[i] = **nextline;
		(*nextline)++;
		i++;
	}
	return (unsigned long long int)strtod(num, NULL);
}
static unsigned long long int chg_from_line(char *nextline)
{
	unsigned long long int ins, del, chged;

	ins = __chg_from_line(&nextline);
	del = __chg_from_line(&nextline);

	chged = ins + del;
	return chged;
}

/*
 * Original format:  "16.5% arch/x86/include/asm/"
 * Return format: "arch/x86/"
 */
static int dir_from_line(char **dir, char *line)
{
	int len;
	char *substr1, *substr2;

	while((*line) != '\0' && (*line) != '%')
		line++;
	line += 2;

	substr1 = strtok(line, "/");
	substr2 = strtok(NULL, "/");
	len = strlen(substr1) + strlen(substr2);
	if ((substr2 != NULL) && (*substr2 != '\n'))
		len += 2;
	else
		len += 1;
	if (init_str(dir, len+1))
		return -1;
	memcpy(*dir, (const char *)substr1, strlen(substr1));
	*(*dir+strlen(substr1)) = '/';
	if ((substr2 != NULL) && (*substr2 != '\n')) {
		memcpy(*dir+strlen(substr1)+1, (const char *)substr2,
			strlen(substr2));
		*(*dir+strlen(substr1)+1+strlen(substr2)) = '/';
	}

	return 0;	
}

int is_author(char *line)
{
	regex_t re;
	const char *pattern = "^Author: .* <.*@.*>";
	int n;

	n = regcomp(&re, pattern, 0);
	if (n != 0)
		return -1;

	n = regexec(&re, line, 0, NULL, 0);
	regfree(&re);

	if (n == 0)
		return 1;
	else
		return 0;
}

int is_diffstat(char *line)
{
	regex_t re;
	const char *pattern = ".*files changed.*insertions.*deletions.*";
	int n;

	n = regcomp(&re, pattern, 0);
	if (n != 0)
		return -1;

	n = regexec(&re, line, 0, NULL, 0);
	regfree(&re);

	if (n == 0)
		return 1;
	else
		return 0;
}

int is_dirstat(char *line)
{
	regex_t re;
	const char *pattern = ".*% .*/\n$";
	int n;

	n = regcomp(&re, pattern, 0);
	if (n != 0)
		return -1;

	n = regexec(&re, line, 0, NULL, 0);
	regfree(&re);

	if (n == 0)
		return 1;
	else
		return 0;
}

int str_conv(const char *org, char **new, const char *from, const char *to)
{
	int i;
	char *tmp;

	if (!org)
		return 1;

	*new = (char *)malloc(256);
	if (!(*new)) {
		printf("[ERROR]: No enough mem\n");
		return 1;
	}
	memset(*new, (int)'\0', 256);
	tmp = *new;

	while (*org != '\0') {
		if (memcmp(org, from, strlen(from)) == 0) {
			for (i = 0; i < strlen(from); i++)
				org++;
			for (i = 0; i < strlen(to); i++) {
				*tmp = to[i];
				tmp++;
			}
		} else {
			*tmp = *org;
			tmp++;
			org++;
		}
	}

	return 0;
}

/*
 * If @from includes sql reversed char, convert the char to what legal to sql.
 * @*char need to be freed by caller.
 */
int sql_str(char *from, char **to)
{
	int len = 0, offset = 0;
	char *tmp = from;

	while (*from != '\0') {
		len++;
		/* FIXME: any other reserved char? --WCN */
		/* ' converts to \'\' */
		if (*from == '\'')
			offset += 3;
		from++;
	}
	/* Restore what from was */
	from = tmp;

	*to = (char *)malloc(len+offset+1);
	if (!(*to)) {
		printf("[ERROR]: No enough mem\n");
		return 1;
	}
	memset(*to, (int)'\0', len+offset+1);

	tmp = *to;
	while (*from != '\0') {
		if (*from == '\'') {
			**to = '\\';
			(*to)++;
			**to = *from;
			(*to)++;
			**to = '\\';
			(*to)++;
			**to = *from;
		} else
			**to = *from;
		(*to)++;
		from++;
	}
	*to = tmp;
	return 0;
}
#endif
