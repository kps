/* Convert a mail address or domain or name to country.
 */
struct conv_country conv_country[] = {
	CONV_CNT("wangchen@cn.fujitsu.com", "Chinese"),
	CONV_NAME_CNT("Johansson", "Swede"),
	CONV_CNT(NULL, NULL)
};
