CFLAG = -Wall
OBJS = main.o
HEAD =  string.h list.h output.h cmdline.h patch_sta.h conv.h allconv.h nation.h Makefile
CC = gcc

all: $(OBJS)
	$(CC) $(OBJS) -o kps $(CFLAG)

main.o: main.c $(HEAD)
	$(CC) -c $< -o $@ $(CFLAG)

clean:
	rm *.o patch_sta -f
	rm ./kps_web/kps_result/*.html -f
	rm ./kps_web/kps_result/*.sql -f
	rm ./kps_web/kps_result/*.tgz -f
	rm ./kps_web/kps_result/*.h -f
	rm ./kps_web/kps_result/*.png -f
	rm ./kps_web/kps_result/*.js -f
	rm ./kps_web/kps_result/nation.inc.php -r
	rm ./kps_web/*.tgz -rf
	rm ./kps_web/kps_result/index.php -f
