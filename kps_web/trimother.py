#!/usr/bin/python

import sys, os
if len(sys.argv) != 3:
	print "./a.py [Filename] [keyword]"
	sys.exit(1)

if not os.path.isfile(sys.argv[1]):
	print 'File "'+sys.argv[1]+'" is not existent.'
	sys.exit(1)

allfile=file(sys.argv[1], "r").read().splitlines()

report=[
'reported-by',
'reported-and-',
'requested-by',
'bisected-by',
'pointed-out-by',
'debugged-by',
'noticed-by',
'found-by',
'spotted-by',
'noted-by',
'diagnosed-by',
'inspired-by',
'report-by',
'pointed-to-by',
'located-by',
'idea-by',
'discovered-by',
'sight-catched-by',
'requested-and-'
]

review=[
'reviewed-by',
'reviewd-by',
'rewieved-by',
'reviewed-off-by',
'reveiewed-by'
]

ack=[
'acked-by',
'acked-off-by',
'acked-and-',
'ackde-by',
'ack-by'
]

test=[
'tested-by',
'tested-and-',
'testted-by',
'verified-by',
'test-by',
'reproduced-by'
]

sof=[
'signed-off-by',
'signed-of-by',
'singed-off-by',
'fixed-by',
'submitted-by',
'written-by',
'sighed-off-by',
'signed-by',
'modified-by',
'signed_off-by',
'sigend-off-by',
'ssigned-off-by',
'sogned-off-by',
'sign-off-by',
'signe-off-by',
'signen-off-by',
'signef-off-by',
'signed-pff-by',
'signed-ff-by',
'siged-off-by'
]

keyset= [report, review, ack, test, sof]
wordkey = ["report","review","ack","test","sof"]

import re
def istime(ist):
	p=re.compile('[1-2][0-9]{2}[0-9]-[0-9]{2}-[0-9]{2}')
	m = p.match(ist)
	if m != None and (m.end() - m.start()) == len(ist):
		return True
	else:
		return False

def isstart(iss,ist):
	p=re.compile('^'+r'[^\s]*'+ist+'.*'+":", re.IGNORECASE)
	if p.match(iss) != None:
		return True
	else:
		return False

time = ''
if wordkey.count(sys.argv[2]) > 0:
	for i in allfile:
		tmp = i.strip()
		if istime(tmp):
			time = tmp
			continue
		if i.count('by:') == 0:
			continue
		for key in keyset[wordkey.index(sys.argv[2])]:
			if len(time) > 0 and isstart(i, key):
				print i+'; '+'Date: '+time
				break
