#!/usr/bin/python

import sys,os

usage='''
Usage: python first-time.py -v [num] -d [source path]
         -v : kernel version, script will get the first time committer of this release.
         -d : kernel's committer info.
'''
if len(sys.argv) != 5:
	print usage 
	sys.exit(1)

dir = ""
for i in sys.argv:
	if dir == "-d":
		dir = i
		break
	if i == "-d":
		dir = "-d"

if dir == "" or dir == "-d" or not os.path.isdir(dir):
	print "directory is unexistent."
	print usage
	sys.exit(1)

files = os.walk(dir).next()[2]
files.sort()
vlist = []
for i in files:
	vlist.append(i.split("_")[0])

ver = ""
for i in sys.argv:
	if ver == "-v":
		ver = i
		break
	if i == "-v":
		ver = "-v"

if ver == "" or ver == "-v" or vlist.count(ver) == 0:
	print "version is unexistent."
	print usage
	sys.exit(1)

filelist = []
for i in range(vlist.index(ver)+1):
	filelist.append(files[i])

allname = [[],[]]
import re
p = re.compile(r'<[\w\s!-]* \(\) [\w\s!-]*>')
def findfile(fname):
	for i in file(fname,'r').read().splitlines():
		line = i.strip()
		m=p.search(line)
		if m != None:
			m=line[m.start():m.end()]
			if allname[1].count(m) == 0:
				allname[0].append(line)
				allname[1].append(m)

for i in filelist[0:-1]:
	findfile(dir+'/'+i)

newline = [[],[]]
for i in file(dir+'/'+filelist[-1],'r').read().splitlines():
	line = i.strip()
	m=p.search(line)
	if m != None:
		m=line[m.start():m.end()]
		if allname[1].count(m) == 0 and newline[1].count(m) == 0:
			newline[0].append(line)
			newline[1].append(m)

#fd=file("result."+ver,"w")

p1=re.compile('\t [^<]* <')
p2=re.compile('\([a-zA-Z ]*\)')
for i in range(len(newline[1])):
	m1=p1.search(newline[0][i])
	m2=p2.search(newline[0][i])
	if m1 != None and m2 != None:
		print newline[0][i][m1.start()+2:]

#following code will support output to a file
#fd.write(newline[0][i][m1.start()+2:m1.end()-2]+"\t"+newline[1][i]+"\t"+newline[0][i][m2.start():m2.end()]+"\n")
#fd.close()
