#!/bin/sh

if [ -r $1 -a -w $2 ]; then
	echo "begin to create history changelog"
	readonly LGP=$1
	KGP=$2
	KGP=${KGP%%/}
else
	echo "usage: $0 linux_kernel_source_dir kps_source_dir"
	exit 1
fi

#versions=""

cd $LGP

echo "create changelog for 2.6.12"
git log -M --date=short --pretty=format:"Author: %aN <%ae>; Date: %ad" --shortstat --dirstat --no-merges v2.6.12-rc2..v2.6.12 >/tmp/ChangeLog-2.6.12
git log v2.6.12-rc2..v2.6.12 --date=short --pretty=format:"%ad%n%b"  --no-merges >/tmp/ChangeLog-2.6.12-other

loop=""
pre=""
for loop in $versions
do
	if [ "$pre" = "" ]
	then
		pre=$loop
		continue
	fi
	echo "create changelog for $loop"
	git log -M --date=short --pretty=format:"Author: %aN <%ae>; Date: %ad" --shortstat --dirstat --no-merges v$pre..v$loop >/tmp/ChangeLog-$loop
	git log v$pre..v$loop --date=short --pretty=format:"%ad%n%b"  --no-merges >/tmp/ChangeLog-$loop-other
	pre=$loop
done

loop=""
for loop in $versions
do
echo "create other statistic changelog for $loop"
$KGP/kps_web/trimother.py /tmp/ChangeLog-$loop-other "report" > /tmp/ChangeLog-$loop-report
$KGP/kps_web/trimother.py /tmp/ChangeLog-$loop-other "review" > /tmp/ChangeLog-$loop-review
$KGP/kps_web/trimother.py /tmp/ChangeLog-$loop-other "test" > /tmp/ChangeLog-$loop-test
$KGP/kps_web/trimother.py /tmp/ChangeLog-$loop-other "ack" > /tmp/ChangeLog-$loop-ack
$KGP/kps_web/trimother.py /tmp/ChangeLog-$loop-other "sof" > /tmp/ChangeLog-$loop-sof
done
