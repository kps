CREATE TABLE `kps` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'db id for indentify',
`name` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'item name',
`class` TINYINT NOT NULL COMMENT '0:people,1:company',
`release` VARCHAR( 16 ) NOT NULL COMMENT 'release name',
`set_num` DOUBLE NOT NULL COMMENT 'patch count',
`line_num` BIGINT NOT NULL COMMENT 'line count',
`employer` VARCHAR( 64 ) NULL COMMENT 'peolple employer'
) ENGINE = MYISAM CHARACTER SET utf8 COLLATE utf8_general_ci;
