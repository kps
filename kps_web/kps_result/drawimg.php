<?php
include 'core.inc.php';

function drawrectangle($image, $xstart, $ystart, $xend, $yend, $color)
{
	imageline($image, $xstart, $ystart, $xend, $ystart, $color);
	imageline($image, $xend, $ystart, $xend, $yend, $color);
	imageline($image, $xend, $yend, $xstart, $yend, $color);
	imageline($image, $xstart, $yend, $xstart, $ystart, $color);
}

function drawpoint($image, $cx, $cy, $width, $color)
{
	imagefilledellipse($image, $cx, $cy, $width, $width, $color);
}

Header("Content-type: image/gif");
$diagramwidth = 1200;
$diagramheight = 400;
$framexstart=0;
$frameystart=0;
$framexend=$diagramwidth-1;
$frameyend=$diagramheight-1;
$image = imageCreate($diagramwidth, $diagramheight);
$colorbackgr = imageColorAllocate($image, 255, 255, 255);
$colorpanel = imageColorAllocate($image, 240, 250, 150);
$colorgrid = imageColorAllocate($image, 20, 80, 80);
$colorset = imageColorAllocate($image, 240, 10, 50);
$colorline = imageColorAllocate($image, 50, 10, 240);
imagefilledrectangle($image, 0, 0, $diagramwidth+1, $diagramheight+1, $colorbackgr);
/* draw a frame */
drawrectangle($image, $framexstart, $frameystart, $framexend, $frameyend, $colorgrid);

if($_REQUEST["action"]=="commit"){
	$title = "Commit Count";
	$release = getrelease();
	$xcount = count($release);

	$name=id2name($id);

	$value = getinfo($name, $release, "set_num");
	$value2 = getinfo($name, $release, "line_num");

	$xcordxstart = $framexstart+100;
	$xcordystart = $frameyend-30;
	$xcordxend = $framexend-100;
	$xcordyend = $xcordystart;

	$ycordxstart = $xcordxstart;
	$ycordystart = $xcordystart;
	$ycordxend = $ycordxstart;
	$ycordyend = $frameystart+30;

	$y2cordxstart = $xcordxend;
	$y2cordystart = $xcordystart;
	$y2cordxend = $y2cordxstart;
	$y2cordyend = $ycordyend;

	/* draw cord */
	imageline($image, $xcordxstart, $xcordystart, $xcordxend, $xcordyend, $colorgrid);
	imageline($image, $ycordxstart, $ycordystart, $ycordxend, $ycordyend, $colorgrid);

	/* draw x grid */
	$xgridwidth = ($xcordxend-$xcordxstart)/($xcount-1);
	for ($i = 0; $i < $xcount; $i++) {
		$xcurrent = $xcordxstart+$i*$xgridwidth;
		$font = 1;
		imageline($image, $xcurrent, $xcordystart,
			   $xcurrent, $xcordystart-3, $colorgrid);
		if ($i%2 == 0)
			$ytmp = $xcordystart+10;
		else
			$ytmp = $xcordystart+2;
		imagestring($image, $font, $xcurrent-10, $ytmp, $release[$i], $colorgrid);
	}

	/* title */
	$font = 5;
	$charwidth = 9;
	$xcurrent =  ($framexend - strlen($title)*$charwidth)/2;
	imagestring($image, $font, $xcurrent, $frameystart+5,
		     $title, $colorgrid);

	imageline($image, $y2cordxstart, $y2cordystart, $y2cordxend, $y2cordyend, $colorgrid);
	$font = 4;
	imagestringup($image, $font, $xcordxstart-70, $ycordyend+200, "Change Sets", $colorset);
	imagestringup($image, $font, $xcordxend+60, $ycordyend+220, "Changed Lines", $colorline);

	/* draw y grid,  Be carefull zero div */
	$ygridheight = ($ycordystart-$ycordyend)/10;
	if (maxofarray($value) <= 10)
		$ypergrid = 1;
	else
		$ypergrid = maxofarray($value)/10;
	if (maxofarray($value2) <= 10)
		$y2pergrid = 1;
	else
		$y2pergrid = maxofarray($value2)/10;
	for ($i = 0; $i < 11; $i++) {
		$ycurrent = $ycordystart-$i*$ygridheight;
		$font = 2;
		/* y cord 1 */
		imageline($image, $ycordxstart, $ycurrent,
			   $ycordxstart+3, $ycurrent, $colorgrid);
		imagestring($image, $font, $ycordxstart-35,
			     $ycurrent-7, intval($i*$ypergrid), $colorset);
		/* y cord 2 */
		imageline($image, $y2cordxstart, $ycurrent,
			   $y2cordxstart-3, $ycurrent, $colorgrid);
		imagestring($image, $font, $y2cordxstart+5,
			     $ycurrent-7, intval($i*$y2pergrid), $colorline);
	}

	/* draw change line of changeset*/
	for ($i = 0; $i < $xcount; $i++) {
		$xstart = $xcordxstart+$i*$xgridwidth;
		$ystart = $ycordystart-$value[$i]*$ygridheight/$ypergrid;
		$point_width = 7;
		drawpoint($image, $xstart, $ystart, $point_width, $colorset);
		if(($i+1) < $xcount) {
			$xend = $xcordxstart+($i+1)*$xgridwidth;
			$yend = $ycordystart-$value[$i+1]*$ygridheight/$ypergrid;
			imageline($image, $xstart, $ystart,
				   $xend, $yend, $colorset);
		}		
	}

	/* draw change line of changedlines*/
	for ($i = 0; $i < $xcount; $i++) {
		$xstart = $xcordxstart+$i*$xgridwidth;
		$ystart = $ycordystart-$value2[$i]*$ygridheight/$y2pergrid;
		$point_width = 7;
		drawpoint($image, $xstart, $ystart, $point_width, $colorline);
		if(($i+1) < $xcount) {
			$xend = $xcordxstart+($i+1)*$xgridwidth;
			$yend = $ycordystart-$value2[$i+1]*$ygridheight/$y2pergrid;
			imageline($image, $xstart, $ystart,
				   $xend, $yend, $colorline);
		}		
	}

	imagepng($image);
	imagedestroy($image);
	exit(0);
}

if($_REQUEST["action"]=="rank"){
	$title = "Commit Rank";
	$release = getrelease();
	$xcount = count($release);

	$name=id2name($id);

	$value = getrank($name, $release, "set_num");
	$value2 = getrank($name, $release, "line_num");
	$max = (maxofarray($value) > maxofarray($value2)) ?
			maxofarray($value): maxofarray($value2);
	$min = (minofarray($value) < minofarray($value2)) ?
			minofarray($value): minofarray($value2);

	$xcordxstart = $framexstart+50;
	$xcordystart = $frameyend-30;
	$xcordxend = $framexend-150;
	$xcordyend = $xcordystart;

	$ycordxstart = $xcordxstart;
	$ycordystart = $xcordystart;
	$ycordxend = $ycordxstart;
	$ycordyend = $frameystart+30;

	$y2cordxstart = $xcordxend;
	$y2cordystart = $xcordystart;
	$y2cordxend = $y2cordxstart;
	$y2cordyend = $ycordyend;

	/* draw cord */
	imageline($image, $xcordxstart, $xcordystart, $xcordxend, $xcordyend, $colorgrid);
	imageline($image, $ycordxstart, $ycordystart, $ycordxend, $ycordyend, $colorgrid);

	/* draw x grid */
	$xgridwidth = ($xcordxend-$xcordxstart)/($xcount-1);
	for ($i = 0; $i < $xcount; $i++) {
		$xcurrent = $xcordxstart+$i*$xgridwidth;
		$font = 1;
		imageline($image, $xcurrent, $xcordystart,
			   $xcurrent, $xcordystart-3, $colorgrid);
		if ($i%2 == 0)
			$ytmp = $xcordystart+10;
		else
			$ytmp = $xcordystart+2;
		imagestring($image, $font, $xcurrent-10, $ytmp, $release[$i], $colorgrid);
	}

	/* title */
	$font = 5;
	$charwidth = 9;
	$xcurrent =  ($framexend - strlen($title)*$charwidth)/2;
	imagestring($image, $font, $xcurrent, $frameystart+5,
		     $title, $colorgrid);

	$font = 4;
	imagefilledrectangle($image, $xcordxend+20, $ycordyend+100,
				$xcordxend+138, $ycordyend+205, $colorpanel);
	imageline($image, $y2cordxstart+30, $ycordyend+110,
		   $y2cordxstart+70, $ycordyend+110, $colorset);
	imageline($image, $y2cordxstart+30, $ycordyend+170,
		   $y2cordxstart+70, $ycordyend+170, $colorline);
	imagestring($image, $font, $xcordxend+30, $ycordyend+120, "Change Sets", $colorset);
	imagestring($image, $font, $xcordxend+30, $ycordyend+180, "Changed Lines", $colorline);

	/* draw y grid */
	$ygridheight = ($ycordystart-$ycordyend)/10;
	if (($max-$min) <= 10)
		$ypergrid = 1;
	else
		$ypergrid = ($max-$min)/10;
	for ($i = 0; $i < 11; $i++) {
		$ycurrent = $ycordystart-$i*$ygridheight;
		$font = 2;
		/* y cord 1 */
		imageline($image, $ycordxstart, $ycurrent,
			   $ycordxstart+3, $ycurrent, $colorgrid);
		imagestring($image, $font, $ycordxstart-35, $ycurrent-7,
			     intval($max-$i*$ypergrid),
			     $colorgrid);
	}

	/* draw change line of changeset*/
	for ($i = 0; $i < $xcount; $i++) {
		$xstart = $xcordxstart+$i*$xgridwidth;
		if ($value[$i] == 0)
			continue;
		$offset = $max-$value[$i];
		$ystart = $ycordystart-($offset*$ygridheight/$ypergrid);
		$point_width = 7;
		drawpoint($image, $xstart, $ystart, $point_width, $colorset);
		/* skip the point which is zero */
		while (($value[$i+1] == 0) && (($i+1) < $xcount)) {
				$i++;
		}
		if(($i+1) < $xcount) {
			$xend = $xcordxstart+($i+1)*$xgridwidth;
			$offset = $max-$value[$i+1];
			$yend = $ycordystart-($offset*$ygridheight/$ypergrid);
			imageline($image, $xstart, $ystart,
				   $xend, $yend, $colorset);
		}		
	}

	/* draw change line of changedlines*/
	for ($i = 0; $i < $xcount; $i++) {
		$xstart = $xcordxstart+$i*$xgridwidth;
		if ($value2[$i] == 0)
			continue;
		$offset = $max-$value2[$i];
		$ystart = $ycordystart-($offset*$ygridheight/$ypergrid);
		$point_width = 7;
		drawpoint($image, $xstart, $ystart, $point_width, $colorline);
		/* skip the point which is zero */
		while (($value2[$i+1] == 0) && (($i+1) < $xcount)) {
				$i++;
		}
		if(($i+1) < $xcount) {
			$xend = $xcordxstart+($i+1)*$xgridwidth;
			$offset = $max-$value2[$i+1];
			$yend = $ycordystart-($offset*$ygridheight/$ypergrid);
			imageline($image, $xstart, $ystart,
				   $xend, $yend, $colorline);
		}		
	}

	imagepng($image);
	imagedestroy($image);
	exit(0);
}

if($_REQUEST["action"]=="who"){
	$title = 'How Many People and Org are Commiting';
	$release = getrelease();
	$xcount = count($release);

	$value = getwho(0, $release);
	$value2 = getwho(1, $release);

	$xcordxstart = $framexstart+100;
	$xcordystart = $frameyend-30;
	$xcordxend = $framexend-100;
	$xcordyend = $xcordystart;

	$ycordxstart = $xcordxstart;
	$ycordystart = $xcordystart;
	$ycordxend = $ycordxstart;
	$ycordyend = $frameystart+30;

	$y2cordxstart = $xcordxend;
	$y2cordystart = $xcordystart;
	$y2cordxend = $y2cordxstart;
	$y2cordyend = $ycordyend;

	/* draw cord */
	imageline($image, $xcordxstart, $xcordystart, $xcordxend, $xcordyend, $colorgrid);
	imageline($image, $ycordxstart, $ycordystart, $ycordxend, $ycordyend, $colorgrid);

	/* draw x grid */
	$xgridwidth = ($xcordxend-$xcordxstart)/($xcount-1);
	for ($i = 0; $i < $xcount; $i++) {
		$xcurrent = $xcordxstart+$i*$xgridwidth;
		$font = 1;
		imageline($image, $xcurrent, $xcordystart,
			   $xcurrent, $xcordystart-3, $colorgrid);
		if ($i%2 == 0)
			$ytmp = $xcordystart+10;
		else
			$ytmp = $xcordystart+2;
		imagestring($image, $font, $xcurrent-10, $ytmp, $release[$i], $colorgrid);
	}

	/* title */
	$font = 5;
	$charwidth = 9;
	$xcurrent =  ($framexend - strlen($title)*$charwidth)/2;
	imagestring($image, $font, $xcurrent, $frameystart+5,
		     $title, $colorgrid);

	imageline($image, $y2cordxstart, $y2cordystart, $y2cordxend, $y2cordyend, $colorgrid);
	$font = 4;
	imagestringup($image, $font, $xcordxstart-70, $ycordyend+200, "How Many People", $colorset);
	imagestringup($image, $font, $xcordxend+60, $ycordyend+220, "How Many Organizitions", $colorline);

	/* draw y grid,  Be carefull zero div */
	$ygridheight = ($ycordystart-$ycordyend)/10;
	/* set max commiter as static */
	$ypergrid = 1800/10;
	/* set max commite employer as static */
	$y2pergrid = 300/10;
	for ($i = 0; $i < 11; $i++) {
		$ycurrent = $ycordystart-$i*$ygridheight;
		$font = 2;
		/* y cord 1 */
		imageline($image, $ycordxstart, $ycurrent,
			   $ycordxstart+3, $ycurrent, $colorgrid);
		imagestring($image, $font, $ycordxstart-35,
			     $ycurrent-7, intval($i*$ypergrid), $colorset);
		/* y cord 2 */
		imageline($image, $y2cordxstart, $ycurrent,
			   $y2cordxstart-3, $ycurrent, $colorgrid);
		imagestring($image, $font, $y2cordxstart+5,
			     $ycurrent-7, intval($i*$y2pergrid), $colorline);
	}

	/* draw change line of changeset*/
	for ($i = 0; $i < $xcount; $i++) {
		$xstart = $xcordxstart+$i*$xgridwidth;
		$ystart = $ycordystart-$value[$i]*$ygridheight/$ypergrid;
		$point_width = 7;
		drawpoint($image, $xstart, $ystart, $point_width, $colorset);
		if(($i+1) < $xcount) {
			$xend = $xcordxstart+($i+1)*$xgridwidth;
			$yend = $ycordystart-$value[$i+1]*$ygridheight/$ypergrid;
			imageline($image, $xstart, $ystart,
				   $xend, $yend, $colorset);
		}		
	}

	/* draw change line of changedlines*/
	for ($i = 0; $i < $xcount; $i++) {
		$xstart = $xcordxstart+$i*$xgridwidth;
		$ystart = $ycordystart-$value2[$i]*$ygridheight/$y2pergrid;
		$point_width = 7;
		drawpoint($image, $xstart, $ystart, $point_width, $colorline);
		if(($i+1) < $xcount) {
			$xend = $xcordxstart+($i+1)*$xgridwidth;
			$yend = $ycordystart-$value2[$i+1]*$ygridheight/$y2pergrid;
			imageline($image, $xstart, $ystart,
				   $xend, $yend, $colorline);
		}		
	}

	imagepng($image);
	imagedestroy($image);
	exit(0);
}
?>
