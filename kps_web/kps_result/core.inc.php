<?php
function db_query_ng($result)
{
	if ($result == FALSE) {
		echo "db error!";
		echo mysql_error();
		exit(1);
	}
}

function runSQL($sql_string, $verify = false)
{
	$db = "";
	$link = "";
	$host = 'localhost';
	$db_name = 'ug7241_db';
	$db_user = 'kps';
	$db_password = 'password';

	$link = mysql_connect($host, $db_user, $db_password);
	db_query_ng($link);

	$res  = mysql_db_query($db_name, $sql_string, $link);
	db_query_ng($res);
	// mysql_db_query returns either positive result ressource or true/false for an insert/update statement
	if ($res === false){
		if ($verify){
			// report DB Problem
			errorpage('Database Problem', mysql_error($link)."\n<br />\n".$sql_string);
		} else {
			// ignore problem but forward the information
			$result = false;	
		}
	}else {
		$result = $res;
	}

	mysql_close($link);
	return $result;
}

function getrelease()
{
	$retarray = array();
	$i = 0;

	$sql = 'select release_ver from kps where name != "end"';
	$result = runSQL($sql);
	db_query_ng($result);
	while ($row = mysql_fetch_array($result)) {
		$row['release_ver'] = $row['release_ver']." ";
		if (!in_array($row['release_ver'], $retarray)) {
			$retarray[$i] = $row['release_ver'];
			$i++;
		}
	}
	while ($i > 0) {
		$i--;
		$retarray[$i] = trim($retarray[$i]);
	}
	return array_reverse($retarray);
}

/* Store name's info to $number
 * 
 * $id: name of people or org
 * $release: array of release info
 * $key: what kind of info we store, example: set_num | line_num
 *
 * return: store the query result into it
 */
function getinfo($name, $release, $key)
{
	$number = array();
	$count = count($release);
	for ($i = 0; $i < $count; $i++) {
		$number[$i] = 0;
		if ($name != NULL) {
			$sql = 'select SUM('.$key.') from kps where name = "'.$name
				.'" and release_ver = "'.$release[$i].'"';
			$result = runSQL($sql);
			db_query_ng($result);
			if (!($row = mysql_fetch_array($result)))
				continue;
			$number[$i] = $row[0];
		} else {
			$sql = 'select * from kps where class = 1 and release_ver = "'.$release[$i].'"';
			$result = runSQL($sql);
			db_query_ng($result);
			while ($row = mysql_fetch_array($result))
				$number[$i] += $row[$key];
		}
	}
	return $number;
}

function getrank($name, $release, $key)
{
	$number = array();
	$count = count($release);
	for ($i = 0; $i < $count; $i++) {
		$number[$i] = 0;
		if ($name != NULL) {
			$sql = 'select * from kps where name = "'.$name
				.'" and release_ver = "'.$release[$i].'"';
			$result = runSQL($sql);
			db_query_ng($result);
			if (!($row = mysql_fetch_array($result)))
				continue;
			$thiscount = $row[$key];
			$thisclass = $row['class'];

			$sql = 'select count(*) as amount from kps where class = '.
				$thisclass.' and release_ver = "'.$release[$i].'"'.
				' and '.$key.' > '.$thiscount;
			$result = runSQL($sql);
			db_query_ng($result);
			$row = mysql_fetch_row($result);
			/* If there are x item before this,
			 * this should be ranked as x+1
			 */
			$number[$i] = $row[0]+1;
		} 
	}
	return $number;
}

function getwho($class, $release)
{
	$ret = array();
	$count = count($release);
	for ($i = 0; $i < $count; $i++) {
		$sql = 'select count(id) from kps where class = '.$class
			.' and release_ver = "'.$release[$i].'"';
		$result = runSQL($sql);
		db_query_ng($result);
		$row = mysql_fetch_array($result);
		$ret[$i] = $row[0];

	}
	return $ret;
}

function maxofarray($array)
{
	$ret = 0;
	$count = count($array);
	for ($i = 0; $i < $count; $i++)
		if ($array[$i] > $ret)
			$ret = $array[$i];
	return $ret;
}

function minofarray($array)
{
	$ret = $array[0];
	$count = count($array);
	for ($i = 1; $i < $count; $i++)
		if ($array[$i] < $ret)
			$ret = $array[$i];
	return $ret;
}

function id2name($id)
{
	$sql = 'select * from kps where id = '.$id;
	$result = runSQL($sql);
	db_query_ng($result);
	if (!($row = mysql_fetch_array($result)))
		return "";
	else
		return $row['name'];
}

function name2id($name)
{
	$sql = 'select * from kps where name = "'.$name.'"';
	$result = runSQL($sql);
	db_query_ng($result);
	if (!($row = mysql_fetch_array($result)))
		return 0;
	else
		return $row['id'];
}
?>
