#!/bin/bash

case $1 in
	"-t")
		MODE="TEXT";
		;;
	"-h")
		MODE="WEB";
		;;
	*)
		echo -e "Error arguments!\n\t./add_back.sh -t/-h filename backaddress";
		echo -e "\t\t-t means text\n\t\t-h means html";
		exit 1;
		;;
esac

if [ ! -f "$2" ]; then
	echo "File missing: $2!" >>/tmp/kps
	exit 1
fi

if [ -z "$3" ]; then
	echo "Please input Home address."
	exit 1
fi

if [ $MODE = "TEXT" ]; then
	sed -i 's/</\&lt;/g' $2
	sed -i 's/>/\&gt;/g' $2
	sed -i '1i<html><A href="'$3'">Back to home</A>\n<pre>' $2
	echo "</pre></html>" >> $2
	cp ./google_adsense /tmp/google_adsense
	cat $2 >> /tmp/google_adsense
	mv /tmp/google_adsense $2
	exit 0
fi

if [ $MODE = "WEB" ]; then
	addr=`echo $3 | sed 's/\//\\\\\//g'`
	grep '<body>' $2 >/dev/null && sed -i 's/<body>/<body>\n<A href="'$addr'">Back to home<\/A>/g' $2
	grep '<BODY>' $2 >/dev/null && sed -i 's/<BODY>/<BODY>\n<A href="'$addr'">Back to home<\/A>/g' $2
	cp ./google_adsense /tmp/google_adsense
	cat $2 >> /tmp/google_adsense
	mv /tmp/google_adsense $2
	exit 0
fi
