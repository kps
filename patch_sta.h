#ifndef INC_PATCH_STA
#define INC_PATCH_STA

#include "list.h"
#include "string.h"
#include "conv.h"

#define OPT_OK 0
#define OPT_ERROR 2
#define SELECT 1
#define NSELECT 0

/* Some systems do not define EXIT_*, despite otherwise supporting C89 */
#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS
#endif

/* FIXME -- WCN : Currently not support corporation statistic */
#ifdef CONFIG_CORP
struct corp {
	char *name;
	int cnt;
	struct company *child;
	struct corp *next;
	struct corp *pre;
};
#endif

struct company {
	struct list_head list;
	char *name;
	int cnt;
	unsigned long long int chgline;
#ifdef CONFIG_CORP
	struct corp *parent;
#endif
	struct list_head *child;
	struct list_head *dir;
};

struct country {
	struct list_head list;
	char *name;
	int cnt;
	unsigned long long int chgline;
	struct list_head *child;
	struct list_head *dir;
};

struct person {
	struct list_head list;
	struct list_head cnt_list;
	char *name;
	struct company *parent;
	struct country *nation;
	int cnt;
	unsigned long long int chgline;
	char *country;
};

/* to find out employers interest in which area */
struct directory {
	struct list_head list;
	char *name;
	int cnt;
	/* dummy field */
	unsigned long long int chgline;
};

struct list_head *cnty_list;

/* The name this program was run with. */
char *program_name;
char *filename_i;
int g_help = NSELECT;
int g_all = NSELECT;
int g_comtop = NSELECT;
int g_com = NSELECT;
int g_tail = NSELECT;
int g_person = NSELECT;
int g_nation = NSELECT;
int g_pertop = NSELECT;
int g_html = NSELECT;
int g_cnty = NSELECT;
int g_sql = NSELECT;
int g_notconv = NSELECT;
int g_line = NSELECT;
int g_interest = NSELECT;
int g_nat_interest = NSELECT;
/* Other statistics, such as: Reported-by, Reviewed-by, Tested-by, Acked-by */
int g_other = NSELECT;
int g_warning = NSELECT;
int g_nospam = NSELECT;
double p_comtop;
char *p_com;
char *p_person;
char *p_nation;
double p_pertop;
double p_tail;
char *p_html;
char *p_cnty;
char *p_nat_interest;
char *p_sql;
char *p_other;
char *p_rls="linux";	/* set it to "linux" as default */
const char *html_tmpl_h = "./html_tmpl_h.html";
const char *html_tmpl_f = "./html_tmpl_f.html";
const char *tmp_author_file = "./tmp_author";
const char *BUGREPORT = "ellre923@gmail.com";

/* what kind of statistics */
int other;
#define OTHER_REPORT 0
#define OTHER_REVIEW 1
#define OTHER_TEST 2
#define OTHER_ACK 3
#define OTHER_SOF 4

const char *str_line		= "changed lines";
const char *str_patch	= "patch sets";
const char *str_report	= "Reported-by";
const char *str_review	= "Reviewed-by";
const char *str_test		= "Tested-by";
const char *str_ack		= "Acked-by";
const char *str_sof		= "Signed-off-by";
char statistics_msg[32];

#define STRUCT_INIT(type) (type *)malloc(sizeof(type))

#define NODE_INIT(type, node) do { \
	node = STRUCT_INIT(type); \
	if (node == NULL) {	\
		printf("[ERROR]: No enough mem\n"); \
		return 1; \
	} \
	memset(node, 0, sizeof(type)); \
	INIT_LIST_HEAD(&(node->list)); } while (0)

#define NODE_SET_NAME(node, str) do {\
	node->name = (char *)malloc(strlen(str)+1);\
	if (node->name == NULL) {\
		printf("[ERROR]: No enough mem\n"); \
		return 1; \
	} \
	memcpy(node->name, str, strlen(str)+1); } while (0)

#define NODE_SET_CNT(node, str) do {\
	node->country = (char *)malloc(strlen(str)+1);\
	if (node->country == NULL) {\
		printf("[ERROR]: No enough mem\n"); \
		return 1; \
	} \
	memcpy(node->country, str, strlen(str)+1); } while (0)

#define LIST_RESORT(head, list_node, type, member) do {\
	struct list_head *list_cur; \
	type *node = list_entry(list_node, type, member); \
	int cnt = node->cnt; \
	int chgline = node->chgline; \
	list_for_each(list_cur, head) { \
		type *node_cur = list_entry(list_cur, type, member); \
		if (g_line == SELECT) { \
			if (node_cur->chgline <= chgline) \
				break; \
		} else { \
			if (node_cur->cnt <= cnt) \
				break; \
		} \
	} \
	if (list_node != list_cur) \
		list_move_tail(list_node, list_cur); } while (0)

/* Update the child's info of the com. */
static void update_child(struct person *per_cur, unsigned long long int chgline)
{
	per_cur->cnt++;
	per_cur->chgline += chgline;
	LIST_RESORT(per_cur->parent->child, &(per_cur->list),
		     struct person, list);
}

/* Update the dir list of nation */
static int update_cnty_dir(struct country *cnty, struct list_head *dir_list)
{
	struct directory *dir_current, *dir, *dir_current2;

	if (list_empty(dir_list))
		return 0;

	list_for_each_entry(dir_current, dir_list, list) {
		list_for_each_entry(dir_current2, cnty->dir, list)
			if (strcasestr(dir_current2->name, dir_current->name))
				break;
		if (&dir_current2->list == cnty->dir) {
			/* new directory */
			NODE_INIT(struct directory, dir);
			NODE_SET_NAME(dir, dir_current->name);
			dir->cnt = 1;
			list_add_tail(&(dir->list), cnty->dir);
		} else {
			struct list_head *cur_dir_list = &(dir_current2->list);
			dir_current2->cnt++;
			LIST_RESORT(cnty->dir, cur_dir_list, struct directory, list);
		}
	}
	return 0;
}

/* Update the person's info in the global per list. */
static void update_per(struct person *per_cur, struct list_head *per_list,
			  unsigned long long int chgline,
			  struct list_head *dir_list)
{
	per_cur->cnt++;
	per_cur->chgline += chgline;
	LIST_RESORT(per_list, &(per_cur->list), struct person, list);

	per_cur->nation->cnt++;
	per_cur->nation->chgline += chgline;
	update_cnty_dir(per_cur->nation, dir_list);
	LIST_RESORT(per_cur->nation->child, &(per_cur->cnt_list),
		    struct person, cnt_list);
	LIST_RESORT(cnty_list, &(per_cur->nation->list), struct country, list);
}

/**
 * new_per: Create and add a per node to com's child list. If the global per
 *	     list has no this per node, create and add the same node to global
 *	     per list.
 * @com_cur:	The com node which should link the new per node.
 * @per_list:	The global per list.
 * @per_name:	The person's name.
 * @chgline:	The commit's changed lines.
 * @dir_list:	The patch's dir list used for nation dir list update.
 * Return: 0 - successful
 *	    1 - not successful
 */
static int new_per(struct company *com_cur, struct list_head *per_list,
		     char *per_name, unsigned long long int chgline,
		     struct list_head *dir_list)
{
	struct person *per;
	char *per_cnt = NULL;
	char *mail_name = NULL;
	char *name = NULL;
	struct person *per_current;
	struct country *cnty;

	/* create a new person node which will be added to the com */
	NODE_INIT(struct person, per);
	NODE_SET_NAME(per, per_name);
	conv_cnt(per_name, &per_cnt);
	NODE_SET_CNT(per, per_cnt);
	per->parent = com_cur;
	per->cnt++;
	per->chgline += chgline;

	/* find the nation in global list */
	list_for_each_entry(cnty, cnty_list, list)
		if (memcmp(cnty->name, per_cnt, strlen(per_cnt)+1)
		    == 0)
			break;
	if (&(cnty->list) == cnty_list) {
		/* create a new nation */
		NODE_INIT(struct country, cnty);
		NODE_SET_NAME(cnty, per_cnt);
		/* alloc mem for child list */
		cnty->child = STRUCT_INIT(struct list_head);
		INIT_LIST_HEAD(cnty->child);
		/* init dir list */
		cnty->dir = STRUCT_INIT(struct list_head);
		INIT_LIST_HEAD(cnty->dir);
		list_add_tail(&(cnty->list), cnty_list);
	}
	per->nation = cnty;

	/* add the person to company's per_list */
	list_add_tail(&(per->list), com_cur->child);
	LIST_RESORT(com_cur->child, &(per->list), struct person, list);

	/* get mail from author info */
	if (mail_from_per(&mail_name, per_name) < 0)
		return 1;
	/* get person's name */
	if (name_from_per(&name, per_name) < 0)
		return 1;
	/* lookup person in global per list */
	list_for_each_entry(per_current, per_list, list)
		if (strcasestr(per_current->name, mail_name))
			break;
		else if ((g_warning == SELECT) &&
			  strcasestr(per_current->name, name)) {
			char *mail_from = NULL, *mail_to = NULL;
			mail_from_per(&mail_from, per_current->name);
			mail_from_per(&mail_to, per_name);
			fprintf(stderr,
				"WARNING: %s and %s might same person.\n",
				per_current->name, per_name);
			fprintf(stderr, "Patch number is %d.\n",per_current->cnt);
			fprintf(stderr,
				"Suggestion: CONV_PER(\"%s\", \"%s\", NULL),\n",
				mail_from, mail_to);
			free(mail_from);
			free(mail_to);
		}
	if (&per_current->list == per_list) {
		/* before using the pointer again, set it to NULL */
		per = NULL;
		NODE_INIT(struct person, per);
		NODE_SET_NAME(per, per_name);
		NODE_SET_CNT(per, per_cnt);
		per->parent = com_cur;
		per->cnt++;
		per->chgline += chgline;
		per->nation = cnty;
		/* add the person to nation's per_list */
		list_add_tail(&(per->cnt_list), cnty->child);
		LIST_RESORT(cnty->child, &(per->cnt_list), struct person, cnt_list);
		/* update the nation list */
		cnty->cnt++;
		cnty->chgline += chgline;
		update_cnty_dir(cnty, dir_list);
		LIST_RESORT(cnty_list, &(cnty->list), struct country, list);
		/* add the person to global per_list */
		list_add_tail(&(per->list), per_list);
		LIST_RESORT(per_list, &(per->list), struct person, list);
	} else {
		update_per(per_current, per_list, chgline, dir_list);
	}

	if (mail_name)
		free(mail_name);
	if (name)
		free(name);
	free(per_cnt);
	return 0;
}

int new_com(struct list_head *com_list, struct list_head *per_list,
	     char *com_name, char *per_name, struct list_head *dir_list,
	     unsigned long long int chgline)
{
	struct company *com;
	struct directory *dir_current, *dir;

	/* create a new company node */
	NODE_INIT(struct company, com);
	NODE_SET_NAME(com, com_name);
	/* alloc mem for child list */
	com->child = STRUCT_INIT(struct list_head);
	INIT_LIST_HEAD(com->child);
	list_add_tail(&(com->list), com_list);

	new_per(com, per_list, per_name, chgline, dir_list);

	/* add directory distribute infor */
	com->dir = STRUCT_INIT(struct list_head);
	INIT_LIST_HEAD(com->dir);
	if (!list_empty(dir_list)) {
		list_for_each_entry(dir_current, dir_list, list) {
			NODE_INIT(struct directory, dir);
			NODE_SET_NAME(dir, dir_current->name);
			dir->cnt = 1;
			list_add_tail(&(dir->list), com->dir);
		}
	}

	/* update the company list */
	com->cnt++;
	com->chgline += chgline;
	LIST_RESORT(com_list, &(com->list), struct company, list);

	return 0;
}

int update_com(struct company *com_cur, struct list_head *com_list,
		struct list_head *per_list, char *per_name,
		struct list_head *dir_list, unsigned long long int chgline)
{
	struct person *per_current;
	char *mail_name = NULL;
	struct directory *dir_current, *dir, *dir_current2;

	/* get mail from person's name */
	if (mail_from_per(&mail_name, per_name) < 0)
		return 1;
	/* search for this person in com's child */
	list_for_each_entry(per_current, com_cur->child, list)
		if (strcasestr(per_current->name, mail_name))
			break;
	if (&per_current->list == com_cur->child) {
		/* the person is new, we add it to list */
		if (new_per(com_cur, per_list, per_name, chgline, dir_list))
			return 1;
	} else {
		/* the person is exist, we update the info of person and com */
		update_child(per_current, chgline);
		/* find the person in global per list and update */
		list_for_each_entry(per_current, per_list, list)
			if (strcasestr(per_current->name, mail_name))
				break;
		if (&per_current->list == per_list)
			return 1;
		else
			update_per(per_current, per_list, chgline, dir_list);
	}

	if (list_empty(dir_list))
		goto no_dir;

	/* update directory distribute infor */
	list_for_each_entry(dir_current, dir_list, list) {
		list_for_each_entry(dir_current2, com_cur->dir, list)
			if (strcasestr(dir_current2->name, dir_current->name))
				break;
		if (&dir_current2->list == com_cur->dir) {
			/* new directory */
			NODE_INIT(struct directory, dir);
			NODE_SET_NAME(dir, dir_current->name);
			dir->cnt = 1;
			list_add_tail(&(dir->list), com_cur->dir);
		} else {
			struct list_head *cur_dir_list = &(dir_current2->list);
			dir_current2->cnt++;
			LIST_RESORT(com_cur->dir, cur_dir_list, struct directory, list);
		}
	}

no_dir:
	/* update the person's company info */
	com_cur->cnt++;
	com_cur->chgline += chgline;
	LIST_RESORT(com_list, &(com_cur->list), struct company, list);

	if (mail_name)
		free(mail_name);
	return 0;
}
#endif
