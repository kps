#ifndef INCLUDE_CMDLINE
#define INCLUDE_CMDLINE

#include <ctype.h>
#include <limits.h>
#include "./patch_sta.h"

#define IS_NUMBER(tmp, char)	\
	while (*tmp != '\0') {	\
		if (!isdigit(*tmp)) {	\
			printf("[ERROR]: -%c should followed "\
				"by a number\n", char);\
			return OPT_ERROR;	\
		}	\
		tmp++;	\
	}

static inline int int_toobig(int num)
{
	if (num < 0)
		return 1;
	return 0;
}

int check_option(char *p)
{
	if (p == NULL)
		return -1;

	/* FIXME -- WCN: I am lazy ... */
	if (*(p+1) != 'a' && *(p+1) != 'c' && *(p+1) != 'C' && *(p+1) != 'm' &&
	    *(p+1) != 'M' && *(p+1) != 'x' && *(p+1) != 'h' && *(p+1) != 'n' &&
	    *(p+1) != 'l' && *(p+1) != 'L' && *(p+1) != 's' && *(p+1) != 'r' &&
	    *(p+1) != 'N' && *(p+1) != 'O' && *(p+1) != 'w' && *(p+1) != 'e' &&
	    *(p+1) != 'I' && *(p+1) != 't' && *(p+1) != 'i')
		return -1;

	return 0;
}

int get_command_option(int argc, char **argv, int index, char *aopt)
{
	int opt;
	char *p;

	p = argv[index];

	if (*p != '-') {
		filename_i = p;
		return 0;
	}

	if (strlen(p) > 2 || (-1 == check_option(p))) {
		printf("[ERROR]: %s Invalidata option!\n", p);
		return -1;
	}

	/* First option should be input file or -h */
	if (index == 1) {
		if (*(p+1) == 'h')
			goto no_opt_check;
		else {
			fprintf(stderr,"[ERROR]: First option "\
				 "should be input file or -h.\n");
			return -1;
		}
	}

	if (aopt[(int)(*(p+1))] == 1) {
		printf("[ERROR]: -%c is repeat!\n", *(p+1));
		return	-1;
	}

no_opt_check:
	opt = *(p+1);
	aopt[opt] = 1;

	if ((argc-1) == index || (*(argv[index+1]) == '-'))
		if ((opt != 'a') && (opt != 'h') && (opt != 'n') &&
		    (opt != 'L') && (opt != 'w') && (opt != 'e') &&
		    (opt != 'I') && (opt != 'i')) {
			printf("[ERROR]: Option -%c lost value!\n", opt);
			return -1;
		}
	return	*(p+1);
}

int parse_command_line(int argc, char **argv)
{
	char aopt[256];
	int index, i;
	int opt;
	char *tmp;

	program_name = argv[0];

	if (argc < 2) {
		printf("[ERROR]: Please give me the input file's name\n");
		return OPT_ERROR;
	}

	index = 0;
	memset(aopt, 0, 256);

	while (1) {
		index++;
		if (index >= argc)
			break;

		opt = get_command_option(argc, argv, index, aopt);
		if (opt ==  -1)
			return OPT_ERROR;

		switch (opt) {
		case	'h':
			g_help = SELECT;
			break;
		case	'a':
			g_all = SELECT;
			break;
		case	'n':
			g_notconv = SELECT;
			break;
		case	'I':
			g_interest = SELECT;
			break;
		case	'L':
			g_line = SELECT;
			break;
		case	'w':
			g_warning = SELECT;
			break;
		case	'e':
			g_nospam = SELECT;
			break;
		case	'C':
			index++;
			g_comtop = SELECT;
			tmp = argv[index];
			IS_NUMBER(tmp, 'C');
			p_comtop = strtod(argv[index], NULL);
			if (int_toobig((int)p_comtop)) {
				printf("TOPX is too big.\n"
					"Should be less than %u\n", INT_MAX);
				return OPT_ERROR;
			}
			break;
		case	'c':
			index++;
			g_com = SELECT;
			p_com = argv[index];
			if ((*p_com) == '-')
				return OPT_ERROR;
			break;
		case	'l':
			index++;
			g_tail = SELECT;
			tmp = argv[index];
			IS_NUMBER(tmp, 'l');
			p_tail = strtod(argv[index], NULL);
			if (int_toobig((int)p_tail)) {
				printf("Tail limit is too big.\n"
					"Should be less than %u\n", INT_MAX);
				return OPT_ERROR;
			}
			break;
		case	'N':
			index++;
			g_nation = SELECT;
			p_nation = argv[index];
			if ((*p_nation) == '-')
				return OPT_ERROR;
			break;
		case	'm':
			index++;
			g_person = SELECT;
			p_person = argv[index];
			if ((*p_person) == '-')
				return OPT_ERROR;
			break;
		case	'M':
			index++;
			g_pertop = SELECT;
			tmp = argv[index];
			IS_NUMBER(tmp, 'M');
			p_pertop = strtod(argv[index], NULL);
			if (int_toobig((int)p_pertop)) {
				printf("TOPX is too big.\n"
					"Should be less than %u\n", INT_MAX);
				return OPT_ERROR;
			}
			break;
		case	'x':
			index++;
			g_html = SELECT;
			p_html = argv[index];
			if ((*p_html) == '-')
				return OPT_ERROR;
			break;
		case	't':
			index++;
			g_cnty = SELECT;
			p_cnty = argv[index];
			if ((*p_cnty) == '-')
				return OPT_ERROR;
			break;
		case	'i':
			index++;
			g_nat_interest = SELECT;
			p_nat_interest = argv[index];
			if ((*p_nat_interest) == '-')
				return OPT_ERROR;
			break;
		case	's':
			index++;
			g_sql = SELECT;
			p_sql = argv[index];
			if ((*p_sql) == '-')
				return OPT_ERROR;
			break;
		case	'r':
			index++;
			p_rls = argv[index];
			if ((*p_rls) == '-')
				return OPT_ERROR;
			break;
		case	'O':
			index++;
			g_other = SELECT;
			p_other = argv[index];
			if ((*p_other) == '-')
				return OPT_ERROR;
			if (strcmp(p_other, "report") == 0) {
				other = OTHER_REPORT;
				break;
			} else if (strcmp(p_other, "review") == 0) {
				other = OTHER_REVIEW;
				break;
			} else if (strcmp(p_other, "test") == 0) {
				other = OTHER_TEST;
				break;
			} else if (strcmp(p_other, "ack") == 0) {
				other = OTHER_ACK;
				break;
			} else if (strcmp(p_other, "sof") == 0) {
				other = OTHER_SOF;
				break;
			} else
				return OPT_ERROR;
		default:
			break;
		}
	}

	if ((g_interest == SELECT || g_nat_interest == SELECT) && g_line == SELECT)
		return OPT_ERROR;

	for (i = 0; i < 256; i++)
		if (aopt[i] == 1)
			goto out;
	g_all = SELECT;

out:
	return OPT_OK;
}

#endif
