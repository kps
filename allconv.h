/* Convert one damain to another.
 * Be sure all convertion should only be following conditions:
 * 1. "from" is one of the sub-domain of "to" and "from" includes more than
 *    one person, otherwise convert this one by person convertion.
 * 2. if "from" is not a sub-domain of "to", it should be a personal domain
 *    belong to one person.
 * 3. any public mail server domain should be "Unknown".
 */
struct conv_com conv_com[] = {
	CONV_COM("audacityteam.org", "Hobbyists"),
	CONV_COM("cmu.edu", "Hobbyists"),
	CONV_COM("deeprootsystems.com", "Consultants"),
	CONV_COM("ducksong.com", "Consultants"),
	CONV_COM("sch.bme.hu", "Academics"),
	CONV_COM("os.inf.tu-dresden.de", "Academics"),
	CONV_COM("amd.com", "AMD"),
	CONV_COM("amd64.org", "AMD"),
	CONV_COM(NULL, NULL)
};

/* Convert person to one mail address and employer
 * Be sure all convertion should only be following conditions:
 * 1. person convertion has higher priority than company convertion.
 * 2. all mails of one person should be convert to unique address.
 * 3. if the domain of "from" is not converted to a unique domain, we should
 *    set "to_com".
 */
struct conv_per conv_per[] = {
	CONV_PER_TIME("<ellre923@gmail.com>", NULL, "Fujitsu", 0, 20091031),
	CONV_PER_TIME("<ellre923@gmail.com>", NULL, "Trend Micro", 20091101, MAXDATE),
	CONV_PER("<wangchen@cn.fujitsu.com>", "<ellre923@gmail.com>", NULL),
	CONV_PER(NULL, NULL, NULL)
};
