#ifndef INC_OUTPUT
#define INC_OUTPUT

#include "patch_sta.h"

int total_num;
int node_cnt;

#define __TOTAL_AVERAGE_PATCH(head, type, member, cat, fp) do {\
	type *cur; \
	node_cnt = 0; \
	total_num = 0; \
	list_for_each_entry(cur, head, member) { \
		node_cnt++;	\
		if (g_line == SELECT) \
			total_num = total_num + cur->chgline;\
		else \
			total_num = total_num + cur->cnt; \
	} \
	if (fp) { \
		fprintf(fp, "Total %s of this kernel release: %u\n",\
			 statistics_msg, total_num);\
		fprintf(fp, "%d %s contribute their works to this kernel release.\n",\
			 node_cnt, cat);\
		fprintf(fp, "Averagely, every %s committed %u %s.\n\n", \
			 cat, total_num?total_num/node_cnt:0, statistics_msg); \
	} else { \
		printf("Total %s of this kernel release: %u\n",	\
			statistics_msg, total_num); \
		printf("%d %s contribute their works to this kernel release.\n",\
			 node_cnt, cat);\
		printf("Averagely, every %s committed %u %s.\n\n", \
			cat, total_num?total_num/node_cnt:0, statistics_msg); \
	} \
} while (0)

#define TOTAL_AVERAGE_PATCH(head, type, member, cat) \
	__TOTAL_AVERAGE_PATCH(head, type, member, cat, NULL)

#define TOTAL_AVERAGE_PATCH_F(head, type, member, cat, fp) \
	__TOTAL_AVERAGE_PATCH(head, type, member, cat, fp)

#define GET_RANK(real_rank, rank, node_cur, head, type, member) do {\
		type *node_prev;\
		rank++;\
		if (node_cur->list.prev != head) {\
			node_prev = list_entry(node_cur->member.prev,\
						  type, member);\
			if (g_line == SELECT) { \
				if (node_cur->chgline < node_prev->chgline)\
					real_rank = rank;\
			} else {\
				if (node_cur->cnt < node_prev->cnt)\
					real_rank = rank;\
			} \
		} else\
			real_rank = rank; } while (0)

/* It's not good to show the emails by text on Internet.
 * Convert email address to coded one.
 * This will convert all '@' to " () " and '.' to " ! ".
 */
char nospamstr[256];
static char *anti_spam(char *orig)
{
	char *p = nospamstr;

	if (g_nospam && orig) {
		/* this loop just cp the name */
		while (*orig != '\0' && *orig != '<') {
			*p = *orig;
			orig++;
			p++;
		}
		/* this loop convert email address */
		while (*orig != '\0') {
			if (*orig == '@') {
				*p = ' ';
				p++;
				*p = '(';
				p++;
				*p = ')';
				p++;
				*p = ' ';
			} else if (*orig == '.') {
				*p = ' ';
				p++;
				*p = '!';
				p++;
				*p = ' ';
			} else {
				*p = *orig;
			}
			orig++;
			p++;
		}
		*p = '\0';
		return nospamstr;
	} else {
		return orig;
	}
}

static void print_child(struct list_head *head)
{
	struct person *per_cur;
	int rank = 0, real_rank = 0;

	list_for_each_entry(per_cur, head, list) {
		GET_RANK(real_rank, rank, per_cur, head,
			  struct person, list);
		if (g_line == SELECT)
			printf("  +--No.%d\t%-64s  %llu\n", real_rank,
				anti_spam(per_cur->name), per_cur->chgline);
		else
			printf("  +--No.%d\t%-64s  %d\n", real_rank,
				anti_spam(per_cur->name), per_cur->cnt);
	}
}

static void print_dir(struct list_head *head)
{
	struct directory *dir_cur;
	int rank = 0, real_rank = 0;

	list_for_each_entry(dir_cur, head, list) {
		GET_RANK(real_rank, rank, dir_cur, head,
			  struct directory, list);
		printf("  +--No.%d\t%-64s  %d\n", real_rank,
			dir_cur->name, dir_cur->cnt);
	}
}

void print_all(struct list_head *head)
{
	struct company *com_cur;
	int rank = 0, real_rank = 0;

	TOTAL_AVERAGE_PATCH(head, struct company, list, "company");

	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (g_line == SELECT)
			printf("No.%d\t%s:  %llu(%.2lf%%)\n", real_rank,
				com_cur->name, com_cur->chgline,
				(double)com_cur->chgline*100/total_num);
		else
			printf("No.%d\t%s:  %d(%.2lf%%)\n", real_rank,
				com_cur->name, com_cur->cnt,
				(double)com_cur->cnt*100/total_num);
		print_child(com_cur->child);
	}
}

void print_com_int(struct list_head *head)
{
	struct company *com_cur;
	int rank = 0, real_rank = 0;

	printf("The following is the statistic about which source \
directory venders contribute most.\nIt implies each vender's interest area \
in Linux kernel.\n");
	TOTAL_AVERAGE_PATCH(head, struct company, list, "company");

	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (g_line == SELECT)
			printf("No.%d\t%s:  %llu(%.2lf%%)\n", real_rank,
				com_cur->name, com_cur->chgline,
				(double)com_cur->chgline*100/total_num);
		else
			printf("No.%d\t%s:  %d(%.2lf%%)\n", real_rank,
				com_cur->name, com_cur->cnt,
				(double)com_cur->cnt*100/total_num);
		print_dir(com_cur->dir);
	}
}

void print_com_rank(struct list_head *head, unsigned int topx)
{
	int rank = 0, real_rank = 0;
	struct company *com_cur;
	unsigned long long int topx_total = 0;

	/* output all if topx is 0 */
	if (!topx)
		topx = 0xffffffff; /* evenif it can be larger on 64-bit box */

	TOTAL_AVERAGE_PATCH(head, struct company, list, "company");

	/* get topx's total number */
	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (real_rank > topx)
			break;
		if (g_line == SELECT)
			topx_total += com_cur->chgline;
		else
			topx_total += com_cur->cnt;
	}

	if (topx != 0xffffffff)
		printf("TOP%d companies contribute %.2lf%% of total.\n",
			topx, topx_total?(double)topx_total*100/total_num:0);

	/* init the following var again */
	real_rank = 0;
	rank = 0;

	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (real_rank > topx)
			break;
		if (g_line == SELECT)
			printf("No.%d\t %-32s  %llu(%.2lf%%)\t\n", real_rank,
				com_cur->name, com_cur->chgline,
				(double)com_cur->chgline*100/total_num);
		else
			printf("No.%d\t %-32s  %d(%.2lf%%)\t\n", real_rank,
				com_cur->name, com_cur->cnt,
				(double)com_cur->cnt*100/total_num);
	}
}

void print_com_detail(struct list_head *head, char *com_name)
{
	int exist = 0, rank = 0, real_rank = 0;
	struct company *com_cur;

	TOTAL_AVERAGE_PATCH(head, struct company, list, "company");

	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (strcasestr(com_cur->name, com_name)) {
		  	exist = 1;
			printf("----------------------------------------\n");
			if (g_line == SELECT)
				printf("No.%d\t%-64s  %llu(%.2lf%%)\n",
					real_rank,
					com_cur->name, com_cur->chgline,
				      (double)com_cur->chgline*100/total_num);
			else
				printf("No.%d\t%-64s  %d(%.2lf%%)\n", real_rank,
					com_cur->name, com_cur->cnt,
					(double)com_cur->cnt*100/total_num);
			print_child(com_cur->child);
		}
	}

	if (!exist)
		printf("[WARNING]: company \"%s\" not exist.\n", com_name);
}

void print_per_detail(struct list_head *head, char *per_name)
{
	int rank = 0, exist = 0, real_rank = 0;
	struct person *per_cur;

	TOTAL_AVERAGE_PATCH(head, struct person, list, "person");

	list_for_each_entry(per_cur, head, list) {
		GET_RANK(real_rank, rank, per_cur, head,
			  struct person, list);
		if (strcasestr(per_cur->name, per_name)) {
			exist = 1;
			if (g_line == SELECT)
				printf("No.%d\t%-64s  %llu\n", real_rank,
					anti_spam(per_cur->name), per_cur->chgline);
			else
				printf("No.%d\t%-64s  %d\n", real_rank,
					anti_spam(per_cur->name), per_cur->cnt);
		}
	}

	if (!exist) {
		printf("[WARNING]: Author \"%s\" does not exist.\n", per_name);
		printf("           Please give me correct author name or"\
		       " mail address.\n");
	}
}

void print_per_rank(struct list_head *head, unsigned int topx)
{
	int real_rank = 0, rank = 0;
	struct person *per_cur;
	unsigned long long int topx_total = 0;

	/* output all if topx is 0 */
	if (!topx)
		topx = 0xffffffff; /* evenif it can be larger on 64-bit box */

	TOTAL_AVERAGE_PATCH(head, struct person, list, "person");

	/* get topx's total number */
	list_for_each_entry(per_cur, head, list) {
		GET_RANK(real_rank, rank, per_cur, head,
			  struct person, list);
		if (real_rank > topx)
			break;
		if (g_line == SELECT)
			topx_total += per_cur->chgline;
		else
			topx_total += per_cur->cnt;
	}

	if (topx != 0xffffffff)
		printf("TOP%d people contribute %.2lf%% of total.\n",
			topx, topx_total?(double)topx_total*100/total_num:0);

	/* init the following var again */
	real_rank = 0;
	rank = 0;
	
	list_for_each_entry(per_cur, head, list) {
		GET_RANK(real_rank, rank, per_cur, head,
			  struct person, list);
		if (real_rank > topx)
			break;
		if (g_line == SELECT)
			printf("No.%d\t %-64s %llu(%.2lf%%)\t@%-32s@%s\n",
				real_rank,
				anti_spam(per_cur->name), per_cur->chgline,
				(double)per_cur->chgline*100/total_num,
				per_cur->parent->name, per_cur->country);
		else
			printf("No.%d\t %-64s %d(%.2lf%%)\t@%-32s@%s\n",
				real_rank,
				anti_spam(per_cur->name), per_cur->cnt,
				(double)per_cur->cnt*100/total_num,
				per_cur->parent->name, per_cur->country);
	}
}

void print_per_cnt(struct list_head *head, char *country)
{
	struct person *per_cur;
	int country_total = 0;
	int country_people = 0;

	TOTAL_AVERAGE_PATCH(head, struct person, list, "person");

	list_for_each_entry(per_cur, head, list) {
		if (!strcasestr(per_cur->country, country))
			continue;
		country_people++;
		if (g_line == SELECT)
			country_total += per_cur->chgline;
		else
			country_total += per_cur->cnt;
	}

	printf("%d(%.2lf%%) %s people contribute %d(%.2lf%%) %s.\n\n",
		country_people, country_people?(double)country_people*100/node_cnt:0, country,
		country_total, country_total?(double)country_total*100/total_num:0,
		statistics_msg);
	/* Declare who @country means 
	printf("(NOTE: \"%s\" means who ethnically belongs to %s descent and many different\
 nationalities are represented)\n\n", country, country);
	*/
	list_for_each_entry(per_cur, head, list) {
		if (!strcasestr(per_cur->country, country))
			continue;
		if (g_line == SELECT)
			printf("%-64s %llu(%.2lf%%)\t%-32s\n",
				anti_spam(per_cur->name), per_cur->chgline,
				(double)per_cur->chgline*100/total_num,
				per_cur->parent->name);
		else
			printf("%-64s %d(%.2lf%%)\t%-32s\n",
				anti_spam(per_cur->name), per_cur->cnt,
				(double)per_cur->cnt*100/total_num,
				per_cur->parent->name);
	}
}

static int file2file(const char *input, char *output, const char *mode)
{
	FILE *fp_o, *fp_i;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;

	fp_i = fopen(input, "r");
	if (!fp_i) {
		printf("[ERROR]: Can not open %s.\n", input);
		return 1;
	}

	fp_o = fopen(output, mode);
	if (!fp_o) {
		printf("[ERROR]: Can not open %s.\n", output);
		fclose(fp_i);
		return 1;
	}

	while ((read = getline(&line, &len, fp_i)) != -1)
		fputs(line, fp_o);

	if (line)
		free(line);
	fclose(fp_o);
	fclose(fp_i);

	return 0;
}

static void print_child_html(struct list_head *head, FILE *fp_o)
{
	struct person *per_cur;
	int rank = 0, real_rank = 0;
	char *new_str, *new_str_tmp;

	list_for_each_entry(per_cur, head, list) {
		GET_RANK(real_rank, rank, per_cur, head,
			  struct person, list);
		str_conv(anti_spam(per_cur->name), &new_str_tmp, "<", "&lt;");
		str_conv(new_str_tmp, &new_str, ">", "&gt;");
		if (g_line == SELECT)
			fprintf(fp_o, "<li>No.%d\t%-64s%llu</li>",
				 real_rank, new_str, per_cur->chgline);
		else
			fprintf(fp_o, "<li>No.%d\t%-64s%d</li>",
				 real_rank, new_str, per_cur->cnt);
		if (new_str)
			free(new_str);
		if (new_str_tmp)
			free(new_str_tmp);
	}
}

int print_html(struct list_head *head, char *output)
{
	struct company *com_cur;
	int rank = 0, real_rank = 0;
	FILE *fp_o;

	if (file2file(html_tmpl_h, output, "w"))
		return 1;

	fp_o = fopen(output, "a");
	if (!fp_o) {
		printf("[ERROR]: Can not open %s.\n", output);
		return 1;
	}

	fprintf(fp_o, "<pre>\n");
	TOTAL_AVERAGE_PATCH_F(head, struct company, list, "companies", fp_o);
	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (g_line == SELECT)
			fprintf(fp_o,
				"<li>No.%d\t%-32s%llu(%.2lf%%)",
				real_rank, com_cur->name, com_cur->chgline,
				(double)com_cur->chgline*100/total_num);
		else
			fprintf(fp_o,
				"<li>No.%d\t%-32s%d(%.2lf%%)",
				real_rank, com_cur->name, com_cur->cnt,
				(double)com_cur->cnt*100/total_num);
		fprintf(fp_o, "\t\t<ul>");
		print_child_html(com_cur->child, fp_o);
		fprintf(fp_o, "\t\t</ul>");
		fprintf(fp_o, "\t</li>");
	}
	fprintf(fp_o, "</pre>\n");
	fclose(fp_o);

	if (file2file(html_tmpl_f, output, "a"))
		return 1;

	return 0;
}

static void print_child_html_cnty(struct list_head *head, FILE *fp_o)
{
	struct person *per_cur;
	int rank = 0, real_rank = 0;
	char *new_str, *new_str_tmp;

	list_for_each_entry(per_cur, head, cnt_list) {
		GET_RANK(real_rank, rank, per_cur, head,
			  struct person, cnt_list);
		str_conv(anti_spam(per_cur->name), &new_str_tmp, "<", "&lt;");
		str_conv(new_str_tmp, &new_str, ">", "&gt;");
		if (g_line == SELECT)
			fprintf(fp_o, "<li>No.%d\t%-64s%llu</li>",
				 real_rank, new_str, per_cur->chgline);
		else
			fprintf(fp_o, "<li>No.%d\t%-64s%d</li>",
				 real_rank, new_str, per_cur->cnt);
		if (new_str)
			free(new_str);
		if (new_str_tmp)
			free(new_str_tmp);
	}
}

int print_cnty(struct list_head *head, char *output)
{
	struct country *cnt_cur;
	int rank = 0, real_rank = 0;
	FILE *fp_o;

	if (file2file(html_tmpl_h, output, "w"))
		return 1;

	fp_o = fopen(output, "a");
	if (!fp_o) {
		printf("[ERROR]: Can not open %s.\n", output);
		return 1;
	}

	fprintf(fp_o, "<pre>\n");
	TOTAL_AVERAGE_PATCH_F(head, struct country, list, "countries", fp_o);
	list_for_each_entry(cnt_cur, head, list) {
		GET_RANK(real_rank, rank, cnt_cur, head,
			  struct country, list);
		if (g_line == SELECT)
			fprintf(fp_o,
				"<li>No.%d\t%-32s%llu(%.2lf%%)",
				real_rank, cnt_cur->name, cnt_cur->chgline,
				(double)cnt_cur->chgline*100/total_num);
		else
			fprintf(fp_o,
				"<li>No.%d\t%-32s%d(%.2lf%%)",
				real_rank, cnt_cur->name, cnt_cur->cnt,
				(double)cnt_cur->cnt*100/total_num);
		fprintf(fp_o, "\t\t<ul>");
		print_child_html_cnty(cnt_cur->child, fp_o);
		fprintf(fp_o, "\t\t</ul>");
		fprintf(fp_o, "\t</li>");
	}
	fprintf(fp_o, "</pre>\n");
	fclose(fp_o);

	if (file2file(html_tmpl_f, output, "a"))
		return 1;

	return 0;
}

static void print_dir_html(struct list_head *head, FILE *fp_o)
{
	struct directory *dir_cur;
	int rank = 0, real_rank = 0;
	char *new_str, *new_str_tmp;

	list_for_each_entry(dir_cur, head, list) {
		GET_RANK(real_rank, rank, dir_cur, head,
			  struct directory, list);
		str_conv(dir_cur->name, &new_str_tmp, "<", "&lt;");
		str_conv(new_str_tmp, &new_str, ">", "&gt;");
		fprintf(fp_o, "<li>No.%d\t%-64s%d</li>",
			 real_rank, new_str, dir_cur->cnt);
		if (new_str)
			free(new_str);
		if (new_str_tmp)
			free(new_str_tmp);
	}
}

int print_com_int_html(struct list_head *head, char *output)
{
	struct company *com_cur;
	int rank = 0, real_rank = 0;
	FILE *fp_o;

	if (file2file(html_tmpl_h, output, "w"))
		return 1;

	fp_o = fopen(output, "a");
	if (!fp_o) {
		printf("[ERROR]: Can not open %s.\n", output);
		return 1;
	}

	fprintf(fp_o, "<pre>\n");
	fprintf(fp_o, "The following is the statistic about which source \
directory venders contribute most.\nIt implies each vender's interest area \
in Linux kernel.\n");
	TOTAL_AVERAGE_PATCH_F(head, struct company, list, "company", fp_o);

	list_for_each_entry(com_cur, head, list) {
		GET_RANK(real_rank, rank, com_cur, head,
			  struct company, list);
		if (g_line == SELECT)
			fprintf(fp_o,
				"<li>No.%d\t%-32s%llu(%.2lf%%)",
				real_rank, com_cur->name, com_cur->chgline,
				(double)com_cur->chgline*100/total_num);
		else
			fprintf(fp_o,
				"<li>No.%d\t%-32s%d(%.2lf%%)",
				real_rank, com_cur->name, com_cur->cnt,
				(double)com_cur->cnt*100/total_num);
		fprintf(fp_o, "\t\t<ul>");
		print_dir_html(com_cur->dir, fp_o);
		fprintf(fp_o, "\t\t</ul>");
		fprintf(fp_o, "\t</li>");
	}
	fprintf(fp_o, "</pre>\n");
	fclose(fp_o);

	if (file2file(html_tmpl_f, output, "a"))
		return 1;

	return 0;
}

int print_cnty_int(struct list_head *head, char *output)
{
	struct country *cnt_cur;
	int rank = 0, real_rank = 0;
	FILE *fp_o;

	if (file2file(html_tmpl_h, output, "w"))
		return 1;

	fp_o = fopen(output, "a");
	if (!fp_o) {
		printf("[ERROR]: Can not open %s.\n", output);
		return 1;
	}

	fprintf(fp_o, "<pre>\n");
	TOTAL_AVERAGE_PATCH_F(head, struct country, list, "countries", fp_o);

	list_for_each_entry(cnt_cur, head, list) {
		GET_RANK(real_rank, rank, cnt_cur, head,
			  struct country, list);
		fprintf(fp_o,
			"<li>No.%d\t%-32s%d(%.2lf%%)",
			real_rank, cnt_cur->name, cnt_cur->cnt,
			(double)cnt_cur->cnt*100/total_num);
		fprintf(fp_o, "\t\t<ul>");
		print_dir_html(cnt_cur->dir, fp_o);
		fprintf(fp_o, "\t\t</ul>");
		fprintf(fp_o, "\t</li>");
	}
	fprintf(fp_o, "</pre>\n");
	fclose(fp_o);

	if (file2file(html_tmpl_f, output, "a"))
		return 1;

	return 0;
}

void print_longtail(struct list_head *head, int num)
{
	int amount = 0, total = 0;
	unsigned long long int tail_total = 0;
	struct person *per_cur;

	TOTAL_AVERAGE_PATCH(head, struct person, list, "person");

	list_for_each_entry(per_cur, head, list) {
		if (g_line == SELECT) {
			if (per_cur->chgline <= num) {
				amount++;
				tail_total = tail_total + per_cur->chgline;
			}
			total = total + per_cur->chgline;
		} else {
			if (per_cur->cnt <= num) {
				amount++;
				tail_total = tail_total + per_cur->cnt;
			}
			total = total + per_cur->cnt;
		}
	}

	printf("We have a long tail:\n");

	printf("There are %d people who committed less than %d %s.\n",
		amount, num, statistics_msg);
	printf("But their contribution is about %llu%% of total.\n",
		tail_total?tail_total*100/total:0);
}

int print_sql(struct list_head *head, char *output, char *rls)
{
	struct company *com_cur;
	FILE *fp_o;
	int ret = 0;

	fp_o = fopen(output, "w");
	if (!fp_o) {
		printf("[ERROR]: Can not open %s.\n", output);
		return 1;
	}

	fprintf(fp_o, "<?php\n");

	list_for_each_entry(com_cur, head, list) {
		char *com_name;
		struct person *per_cur;

		if (sql_str(com_cur->name, &com_name) != 0) {
			ret = 1;
			goto out;
		}
		fprintf(fp_o,"$sql_mid .=\'(NULL, \\\'%s\\\', \\\'1\\\', "
			"\\\'%s\\\', \\\'%d\\\', "
			"\\\'%llu\\\', NULL), \';\n",
			com_name, rls,
			com_cur->cnt, com_cur->chgline);

		list_for_each_entry(per_cur, com_cur->child, list) {
			char *per_name;
			if (sql_str(per_cur->name, &per_name) != 0) {
				ret = 1;
				free(com_name);
				goto out;
			}
			fprintf(fp_o,"$sql_mid .=\'(NULL, \\\'%s\\\', "
				"\\\'0\\\', \\\'%s\\\', \\\'%d\\\', "
				"\\\'%llu\\\', \\\'%s\\\'), \';\n",
				per_name, rls,
				per_cur->cnt, per_cur->chgline, com_name);
			free(per_name);
		}
		free(com_name);
	}

	fprintf(fp_o, "?>\n");
out:
	fclose(fp_o);
	return ret;
}
#endif
