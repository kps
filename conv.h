/* Kernel changelog analysis
 * (C) 2008 Wang Chen
 *
 * This code is licenced under the GPL.
 *
 */

#ifndef INC_CONV
#define INC_CONV

#include <ctype.h>

#define MAXDATE 0xffffffff

/* Matching string with people's mail information. Instead of search string in
 * whole mail information, we can strictly demand conv_cnt() to match the
 * string with appointed field of mail information. There are 5 fields:
 * 1.FIRSTNAME, 2.MIDNAME, 3.LASTNAME, 4.ACCOUNT, 5.DOMAIN
 * We can still search the keyword in whole mail info by assigning field to 0
 */
enum mail_field {
ALL,	/* search a string in whole mail infor, match a substr */
FIRST,	/* strictly, but not casesensitively match a string with first name */
MID,	/* strictly, but not casesensitively match a string with mid name */
LAST,	/* strictly, but not casesensitively match a string with last name */
ACCOUNT,/* strictly, but not casesensitively match a string with mail account */
DOMAIN,	/* match a sub string with mail domain */
NAME	/* strictly, but not casesensitively match a string with one word of name */
};

struct conv_country {
	char *mail;
	char *country;
	/* which field of "FIRSTNAME MIDNAME LASTNAME <ACCOUNT@DOMAIN>" should
	 * match the compared keyword.
	 */
	enum mail_field field;
};

struct conv_com {
	char *from;
	char *to;
};

struct conv_per {
	char *from;
	char *to;
	char *to_com;
	/* Some people move to another company but not change mail address.
	 * To known these people's commits attribution to which company, we
	 * need to known the people attribution to where in some time range.
	 */
	unsigned int begin;	/* attribution from when */
	unsigned int end;	/* attribution to when */
	char *name;	/* to fore the name */
};

#define CONV_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = ALL,	\
}

#define CONV_NAME_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = NAME,	\
}

#define CONV_FIRST_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = FIRST,	\
}

#define CONV_MID_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = MID,	\
}

#define CONV_LAST_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = LAST,	\
}

#define CONV_ACCOUNT_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = ACCOUNT,	\
}

#define CONV_DOMAIN_CNT(_mail, _country) {\
	.mail = _mail,	\
	.country = _country,	\
	.field = DOMAIN,	\
}

#define CONV_COM(_from, _to) {\
	.from = _from,	\
	.to = _to,		\
}

/* This define is used when declaring a people who didn't ever change
 * attribution.
 */
#define CONV_PER(_from, _to, _to_com) {\
	.from = _from,	\
	.to = _to,		\
	.to_com = _to_com,	\
	.begin = 0,		\
	.end = 0,		\
	.name = NULL,		\
}

/* This define is used when declaring a people who changed attribution
 * sometime.
 */
#define CONV_PER_TIME(_from, _to, _to_com, _begin, _end) {\
	.from = _from,	\
	.to = _to,		\
	.to_com = _to_com,	\
	.begin = _begin,	\
	.end = _end,		\
	.name = NULL,		\
}

/* This define is used when declaring a people who didn't ever change
 * attribution and need a force name.
 */
#define CONV_PER_NAME(_from, _to, _to_com, _name) {\
	.from = _from,	\
	.to = _to,		\
	.to_com = _to_com,	\
	.begin = 0,		\
	.end = 0,		\
	.name = _name,	\
}

/* This define is used when declaring a people who changed attribution
 * sometime and need a force name.
 */
#define CONV_PER_TIME_NAME(_from, _to, _to_com, _begin, _end, _name) {\
	.from = _from,	\
	.to = _to,		\
	.to_com = _to_com,	\
	.begin = _begin,	\
	.end = _end,		\
	.name = _name,	\
}

inline static int date_ok(unsigned int date, struct conv_per conv_per)
{
	if (date <= conv_per.end && date >= conv_per.begin)
		return 1;
	else
		return 0;
}	

#include "nation.h"
#include "allconv.h"

static int convt_name(char **from, char *name)
{
	char *tmp = NULL;
	char *mail = NULL;
	int len = 0;
	int ret = 0;

	ret = mail_from_per(&mail, *from);
	if (ret < 0)
		return 1;
	len += strlen(mail);
	len += strlen(name);
	/* between name and mail, there is a whitespace */
	len++;

	if (init_str(&tmp, len+1))
		return 1;
	free(*from);
	*from = tmp;

	/* Copy name to new string */
	memcpy(tmp, name, strlen(name));
	/* Copy white space to new string */
	tmp = strchr(*from, '\0');
	memcpy(tmp, " ", 1);
	/* Copy mail to new string */
	tmp = strchr(*from, '\0');
	memcpy(tmp, mail, strlen(mail));

	free(mail);
	return 0;
}

static int convt_per(char **from, char *mail)
{
	char *tmp = *from;
	int len = 0, len_name = 0;

	/* Get len of author's name */
	while ((tmp != NULL) && (*tmp != '\0') &&
		(*tmp != '\n') && (*tmp != '<')) {
		len_name++;
		tmp++;
	}
	len = len_name + strlen(mail);
	if (init_str(&tmp, len+1))
		return 1;
	/* Copy name to new string */
	memcpy(tmp, *from, len_name);
	free(*from);
	*from = tmp;
	/* Copy mail to new string */
	tmp = strchr(*from, '\0');
	memcpy(tmp, mail, strlen(mail));
	return 0;
}

static int convt_com(char **from, char *to)
{
	free(*from);
	if (init_str(from, strlen(to)+1))
		return 1;
	memcpy(*from, to, strlen(to));
	return 0;
}

/* Find out one's country name by his/her mail infor.
 * @per: mail infor as formast(FIRSTNAME MIDNAME LASTNAME <ACCOUNT@DOMAIN>)
 * @cnt: country name string
 */
int conv_cnt(char *per, char **cnt)
{
	int i;
	const char *unknown = "Unknown";
	char *str = NULL;

	for (i = 0; conv_country[i].mail != NULL; i++) {
		switch (conv_country[i].field) {
		case ALL:
			if (strcasestr(per, conv_country[i].mail))
				goto cp_country;
			break;
		case FIRST:
			if (firstname(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			break;
		case MID:
			if (midname(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			break;
		case LAST:
			if (lastname(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			break;
		case ACCOUNT:
			if (account(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			break;
		case DOMAIN:
			if (domain(&str, per))
				if (strcasestr(str, conv_country[i].mail))
					goto cp_country;
			break;
		case NAME:
			if (firstname(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			if (str) {free(str); str = NULL;}
			if (midname(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			if (str) {free(str); str = NULL;}
			if (lastname(&str, per))
				if (strcasecmp(conv_country[i].mail, str) == 0)
					goto cp_country;
			break;
		default:
			break;
		}
		if (str) {free(str); str = NULL;}
	}

cp_country:
	if (str) {free(str); str = NULL;}
	if (conv_country[i].mail != NULL) {
		if (init_str(cnt, strlen(conv_country[i].country)+1))
			return 1;
		memcpy(*cnt, conv_country[i].country,
			strlen(conv_country[i].country));
	} else {
		if (init_str(cnt, strlen(unknown)+1))
			return 1;
		memcpy(*cnt, unknown, strlen(unknown));
	}

	return 0;
}

static void conv_mail2lower(char *per)
{
	while (*per != '<' && *per != '\0') {
		per++;
	}
	do {
		per++;
		*per = tolower(*per);
	} while (*per != '>' && *per != '\0');
}

/**
 * conv_per_com: Convert mail addresses for one person and attribution an
 *		   engineer to employer.
 * @per: Engineer's name and mail address which needs convert. Convert result
 *	  store in it too.
 * @com: Engineer's employer which automaticlly matched but maybe not correct.
 *	  Convert result store in it.
 * @date:This commit's date which used to tell an engineer attribution to which
 *	  employer in that date.
 * Return: 0 - successful
 *	    1 - not successful
 */
int conv_per_com(char **per, char **com, unsigned int date)
{
	int i, ret;
	char *mail;

	conv_mail2lower(*per);		
	if (mail_from_per(&mail, *per) < 0) {
		ret = 1;
		goto out1;
	}

	/* Whether this company needs conversion.
	 * Should do this conversion before mail conversion.
	 */
	for (i = 0; conv_com[i].from != NULL; i++)
		if (strcasestr(*com, conv_com[i].from))
			break;
	if (conv_com[i].from != NULL) {
		if (convt_com(com, conv_com[i].to)) {
			ret = 1;
			goto out;
		}
	} else {
		if (convt_com(com, "Unknown")) {
			ret = 1;
			goto out;
		}
	}

	/* Whether this mail or name needs conversion */
	for (i = 0; conv_per[i].from != NULL; i++) {
		if ( (*mail != '\0') &&
		     (strncasecmp(conv_per[i].from, mail, strlen(mail)) == 0)) {
			/* mail convert */
			if (conv_per[i].to != NULL)
				if (convt_per(per, conv_per[i].to)) {
					ret = 1;
					goto out;
				}
			/* name convert */
			if (conv_per[i].name != NULL)
				if (convt_name(per, conv_per[i].name)) {
					ret = 1;
					goto out;
				}
			/* employer convert */
			if (conv_per[i].to_com != NULL) {
				if ((conv_per[i].begin + conv_per[i].end == 0) ||
				     date_ok(date, conv_per[i])) {
					if (convt_com(com, conv_per[i].to_com)) {
						ret = 1;
						goto out;
					}
					break;
				}
			} else
				break;
		}
	}
out:
	if (mail)
		free(mail);
out1:
	return ret;
}

#endif
